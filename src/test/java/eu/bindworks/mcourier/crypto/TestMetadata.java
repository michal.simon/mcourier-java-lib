package eu.bindworks.mcourier.crypto;

import eu.bindworks.mcourier.SecretFileMetadata;
import org.hamcrest.CoreMatchers;
import org.junit.Assert;
import org.junit.Test;

public class TestMetadata {
    @Test
    public void testSecretFileMetadata() throws Exception {
        SecretFileMetadata md = new SecretFileMetadata();
        md.setInt("abc", 1);
        byte[] bytes = md.serializeToBytes();
        String chars = md.serializeToJson();

        SecretFileMetadata bmd = new SecretFileMetadata();
        bmd.deserializeFromBytes(bytes);
        Assert.assertThat(bmd, CoreMatchers.equalTo(md));

        SecretFileMetadata jmd = new SecretFileMetadata();
        jmd.deserializeFromJson(chars);
        Assert.assertThat(jmd, CoreMatchers.equalTo(md));
    }
}
