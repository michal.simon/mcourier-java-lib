package eu.bindworks.mcourier.crypto;

import eu.bindworks.mcourier.DecryptedShipment;
import eu.bindworks.mcourier.EncryptedShipment;
import eu.bindworks.mcourier.SecretFileMetadata;
import org.hamcrest.CoreMatchers;
import org.junit.Assert;
import org.junit.Test;

import java.io.ByteArrayInputStream;
import java.nio.charset.StandardCharsets;

public class TestEncryptedShipmentEncryptor {
    public static final byte[] TEST_BYTE_STREAM = "Hello world! \u1234".getBytes(StandardCharsets.UTF_8);

    @Test
    public void testShipmentEncryptorInvalidPassword() throws Exception {
        ShipmentEncryptorConfiguration encryptorConfiguration = new ShipmentEncryptorConfiguration();
        ShipmentEncryptor encryptor = new ShipmentEncryptor(encryptorConfiguration);
        try (EncryptedShipmentBuilder cShipment = encryptor.createShipment()) {
            cShipment.encryptFile(new ByteArrayInputStream(TEST_BYTE_STREAM), new SecretFileMetadata()
                    .contentType("application/octet-stream")
                    .description("test description")
                    .filename("abc.txt")
                    .iconUrl("https://d32-a.sdn.cz/d_32/c_static_QO_D/4xABNm/media/img/logo_v2.svg"));

            cShipment.encryptFile(new ByteArrayInputStream(TEST_BYTE_STREAM), new SecretFileMetadata()
                    .contentType("application/octet-stream")
                    .description("test description")
                    .filename("abc.txt")
                    .iconUrl("https://d32-a.sdn.cz/d_32/c_static_QO_D/4xABNm/media/img/logo_v2.svg"));

            cShipment.finalizeShipment("PA55W0RD".getBytes(StandardCharsets.UTF_8), System.currentTimeMillis() + 1000*60*60);
            EncryptedShipment encryptedShipment = cShipment.getEncryptedShipment();
            Assert.assertThat(encryptedShipment, CoreMatchers.notNullValue());

            byte[] encryptedFile1 = cShipment.getInputStreams().get(0).readAllBytes();
            byte[] encryptedFile2 = cShipment.getInputStreams().get(1).readAllBytes();

            ShipmentDecryptor decryptor = new ShipmentDecryptor();
            Assert.assertTrue(decryptor.validateShipment(encryptedShipment));
            for (int i=0; i<encryptedShipment.getHostingShipmentMetadata().getChallengesCount(); i++) {
                Assert.assertFalse(decryptor.isChallengeValid(encryptedShipment, "password1".getBytes(StandardCharsets.UTF_8), i));
            }
        }
    }

    @Test
    public void testShipmentEncryptorVanilla() throws Exception {
        ShipmentEncryptorConfiguration encryptorConfiguration = new ShipmentEncryptorConfiguration();
        ShipmentEncryptor encryptor = new ShipmentEncryptor(encryptorConfiguration);
        try (EncryptedShipmentBuilder cShipment = encryptor.createShipment()) {
            cShipment.encryptFile(new ByteArrayInputStream(TEST_BYTE_STREAM), new SecretFileMetadata()
                    .contentType("application/octet-stream")
                    .description("test description")
                    .filename("abc.txt")
                    .iconUrl("https://d32-a.sdn.cz/d_32/c_static_QO_D/4xABNm/media/img/logo_v2.svg"));

            cShipment.encryptFile(new ByteArrayInputStream(TEST_BYTE_STREAM), new SecretFileMetadata()
                    .contentType("application/octet-stream")
                    .description("test description")
                    .filename("abc.txt")
                    .iconUrl("https://d32-a.sdn.cz/d_32/c_static_QO_D/4xABNm/media/img/logo_v2.svg"));

            cShipment.finalizeShipment("PA55W0RD".getBytes(StandardCharsets.UTF_8), System.currentTimeMillis() + 1000*60*60);
            EncryptedShipment encryptedShipment = cShipment.getEncryptedShipment();
            Assert.assertThat(encryptedShipment, CoreMatchers.notNullValue());

            byte[] encryptedFile1 = cShipment.getInputStreams().get(0).readAllBytes();
            byte[] encryptedFile2 = cShipment.getInputStreams().get(1).readAllBytes();

            ShipmentDecryptor decryptor = new ShipmentDecryptor();
            Assert.assertTrue(decryptor.validateShipment(encryptedShipment));
            Assert.assertTrue(decryptor.isShipmentPasswordValid(encryptedShipment, "password".getBytes(StandardCharsets.UTF_8)));
            DecryptedShipment dShipment = decryptor.decryptShipment(encryptedShipment, "password".getBytes(StandardCharsets.UTF_8));

            Assert.assertThat(dShipment.getFilesCount(), CoreMatchers.equalTo(2));

            SecretFileMetadata firstFile = dShipment.getFileMetadata(0);
            Assert.assertThat(firstFile.getContentType(), CoreMatchers.equalTo("application/octet-stream"));
            Assert.assertThat(firstFile.getDescription(), CoreMatchers.equalTo("test description"));
            Assert.assertThat(firstFile.getFilename(), CoreMatchers.equalTo("abc.txt"));
            Assert.assertThat(firstFile.getIconUrl(), CoreMatchers.equalTo("https://d32-a.sdn.cz/d_32/c_static_QO_D/4xABNm/media/img/logo_v2.svg"));

            byte[] decryptedFile1 = dShipment.decryptFile(new ByteArrayInputStream(encryptedFile1), 0).readAllBytes();
            byte[] decryptedFile2 = dShipment.decryptFile(new ByteArrayInputStream(encryptedFile2), 1).readAllBytes();

            Assert.assertThat(decryptedFile1, CoreMatchers.equalTo(TEST_BYTE_STREAM));
            Assert.assertThat(decryptedFile2, CoreMatchers.equalTo(TEST_BYTE_STREAM));
        }
    }

}
