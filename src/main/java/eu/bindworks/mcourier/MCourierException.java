package eu.bindworks.mcourier;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.ShortBufferException;
import java.io.IOException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;

public class MCourierException extends RuntimeException {
    public MCourierException(String message) {
        super(message);
    }

    public MCourierException(String message, Throwable cause) {
        super(message, cause);
    }

    public static class ConfigurationException extends MCourierException {
        public ConfigurationException(String message) {
            super(message);
        }

        public ConfigurationException(String message, Throwable cause) {
            super(message, cause);
        }
    }

    public static class EncryptionDecryptionException extends MCourierException {
        public EncryptionDecryptionException(String message) {
            super(message);
        }

        public EncryptionDecryptionException(String message, Throwable cause) {
            super(message, cause);
        }
    }

    public static class ShipmentSubmissionException extends MCourierException {
        public ShipmentSubmissionException(String message) {
            super(message);
        }

        public ShipmentSubmissionException(String message, Throwable cause) {
            super(message, cause);
        }
    }

    public static ConfigurationException translate(NoSuchAlgorithmException e) {
        throw new ConfigurationException("Required algorithm not found.", e);
    }

    public static ConfigurationException translate(NoSuchPaddingException e) {
        throw new ConfigurationException("Required padding not found.", e);
    }

    public static EncryptionDecryptionException translate(InvalidKeyException e) {
        throw new EncryptionDecryptionException("Invalid key passed.", e);
    }

    public static EncryptionDecryptionException translate(ShortBufferException e) {
        throw new EncryptionDecryptionException("Short buffer passed. This is fatal.", e);
    }

    public static EncryptionDecryptionException translate(IllegalBlockSizeException e) {
        throw new EncryptionDecryptionException("Illegal buffer size passed. This is fatal.", e);
    }

    public static EncryptionDecryptionException translate(BadPaddingException e) {
        throw new EncryptionDecryptionException("Bad padding exception. This is fatal.", e);
    }

    public static EncryptionDecryptionException translate(NoSuchProviderException e) {
        throw new EncryptionDecryptionException("No such provider -- have you called EncryptionSuite.registerBouncyCastleSecurityProvider()?", e);
    }

    public static EncryptionDecryptionException translate(KeyStoreException e) {
        throw new EncryptionDecryptionException("Keystore exception", e);
    }

    public static EncryptionDecryptionException translate(KeyManagementException e) {
        throw new EncryptionDecryptionException("Key management exception", e);
    }

    public static EncryptionDecryptionException translate(UnrecoverableKeyException e) {
        throw new EncryptionDecryptionException("Unrecoverable key exception", e);
    }

    public static EncryptionDecryptionException translate(CertificateException e) {
        throw new EncryptionDecryptionException("Certificate exception", e);
    }

    public static EncryptionDecryptionException translate(IOException e) {
        throw new EncryptionDecryptionException("IO Error while processing", e);
    }

    public static ConfigurationException translate(InvalidAlgorithmParameterException e) {
        throw new ConfigurationException("Invalid parameter", e);
    }

    @SuppressWarnings({"java:S2147"})
    public static <T> T translate(ReturningExceptionThrower<T> thrower) {
        try {
            return thrower.run();
        } catch (NoSuchAlgorithmException e) {
            throw translate(e);
        } catch (NoSuchPaddingException e) {
            throw translate(e);
        } catch (InvalidKeyException e) {
            throw translate(e);
        } catch (ShortBufferException e) {
            throw translate(e);
        } catch (IllegalBlockSizeException e) {
            throw translate(e);
        } catch (BadPaddingException e) {
            throw translate(e);
        } catch (IOException e) {
            throw translate(e);
        } catch (InvalidAlgorithmParameterException e) {
            throw translate(e);
        } catch (NoSuchProviderException e) {
            throw translate(e);
        } catch (KeyStoreException e) {
            throw translate(e);
        } catch (KeyManagementException e) {
            throw translate(e);
        } catch (UnrecoverableKeyException e) {
            throw translate(e);
        } catch (CertificateException e) {
            throw translate(e);
        }
    }

    public static void translate(VoidExceptionThrower thrower) {
        translate(() -> {
            thrower.run();
            return null;
        });
    }

    @FunctionalInterface
    public interface ReturningExceptionThrower<T> {
        T run() throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException,
                ShortBufferException, IllegalBlockSizeException, BadPaddingException, IOException,
                InvalidAlgorithmParameterException, NoSuchProviderException, KeyStoreException,
                KeyManagementException, UnrecoverableKeyException, CertificateException;
    }

    @FunctionalInterface
    public interface VoidExceptionThrower {
        void run() throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException,
                ShortBufferException, IllegalBlockSizeException, BadPaddingException, IOException,
                InvalidAlgorithmParameterException, NoSuchProviderException, KeyStoreException,
                KeyManagementException, UnrecoverableKeyException, CertificateException;
    }
}
