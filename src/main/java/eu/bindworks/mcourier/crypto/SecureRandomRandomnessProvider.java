package eu.bindworks.mcourier.crypto;

import java.security.SecureRandom;

public class SecureRandomRandomnessProvider implements RandomnessProvider {
    private SecureRandom secureRandom;

    public SecureRandomRandomnessProvider() {
        this(null);
    }

    public SecureRandomRandomnessProvider(SecureRandom secureRandom) {
        this.secureRandom = secureRandom == null ? new SecureRandom() : secureRandom;
    }

    @Override
    public byte[] randomBytes(int count) {
        byte[] bytes = new byte[count];
        secureRandom.nextBytes(bytes);
        return bytes;
    }
}
