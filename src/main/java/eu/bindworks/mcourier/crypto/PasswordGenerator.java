package eu.bindworks.mcourier.crypto;

import eu.bindworks.mcourier.MCourierException;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Objects;

public class PasswordGenerator {
    public static final String NORMALIZER_SEQUENCE = PasswordNormalizerBuilder.NORMALIZER_NAME_UPPERCASE + " "
            + PasswordNormalizerBuilder.NORMALIZER_NAME_REMOVE_DIACRITICS + " "
            + PasswordNormalizerBuilder.NORMALIZER_NAME_NONMIXABLE;

    private PasswordGeneratorConfiguration configuration;

    public PasswordGenerator(PasswordGeneratorConfiguration configuration) {
        Objects.requireNonNull(configuration);

        this.configuration = configuration;
        this.initializeEncryption();
    }

    protected void initializeEncryption() {
        MessageDigest passwordDigestor = createPasswordDigestor();
        if (passwordDigestor.getDigestLength() < configuration.getPasswordLength()) {
            throw new IllegalArgumentException("Password digesting algorithm does not produce enough bytes for " + configuration.getPasswordLength() + " character long password");
        }
    }

    protected MessageDigest createPasswordDigestor() {
        try {
            return MessageDigest.getInstance(configuration.getPasswordDigestionAlgorithm());
        } catch (NoSuchAlgorithmException e) {
            throw MCourierException.translate(e);
        }
    }

    public String getNormalizerSequence() {
        return NORMALIZER_SEQUENCE;
    }

    public ClearableBytes generatePassword() {
        byte[] bytes = configuration.getRandomnessProvider().randomBytes(configuration.getPasswordLength());
        MessageDigest passwordDigestor = createPasswordDigestor();
        for (int i=0; i<configuration.getDigestRounds(); i++) {
            bytes = passwordDigestor.digest(bytes);
        }

        byte[] password = new byte[configuration.getPasswordLength()];
        for (int i=0; i<password.length; i++) {
            password[i] = (byte) PasswordNormalizerBuilder.NONMIXABLE_CHARACTERS.charAt(Math.abs(bytes[i]) % PasswordNormalizerBuilder.NONMIXABLE_CHARACTERS.length());
        }
        return ClearableBytes.take(password);
    }
}
