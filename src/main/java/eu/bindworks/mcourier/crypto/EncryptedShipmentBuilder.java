package eu.bindworks.mcourier.crypto;

import eu.bindworks.mcourier.EncryptedShipment;
import eu.bindworks.mcourier.HostingShipmentMetadata;
import eu.bindworks.mcourier.MCourierException;
import eu.bindworks.mcourier.PublicShipmentMetadata;
import eu.bindworks.mcourier.SecretFileMetadata;
import eu.bindworks.mcourier.SecretShipmentMetadata;
import eu.bindworks.mcourier.Utility;

import java.io.Closeable;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class EncryptedShipmentBuilder implements AutoCloseable, Closeable {
    private ShipmentEncryptorConfiguration configuration;
    private EncryptionSuite encryptionSuite;

    private PublicShipmentMetadata publicShipmentMetadata = new PublicShipmentMetadata();
    private SecretShipmentMetadata secretShipmentMetadata = new SecretShipmentMetadata();
    private HostingShipmentMetadata hostingShipmentMetadata = new HostingShipmentMetadata();

    private ClearableBytes allSecretFileMetadataBytes;

    private byte[] masterFinalSeed;
    private byte[] masterTransformSeed;
    private boolean hasStarted;
    private List<InputStream> inputStreams;
    private EncryptedShipment encryptedShipment;

    EncryptedShipmentBuilder(ShipmentEncryptorConfiguration configuration) {
        this.configuration = configuration;
        encryptionSuite = configuration.getEncryptionSuite();
    }

    public InputStream encryptFile(File file, SecretFileMetadata metadata) {
        Objects.requireNonNull(file);
        SecretFileMetadata secretFileMetadata = metadata == null ? new SecretFileMetadata() : metadata;

        return MCourierException.translate(() -> {
            if (secretFileMetadata.getFilename() == null) {
                secretFileMetadata.setFilename(file.getName());
            }
            if (secretFileMetadata.getContentType() == null) {
                secretFileMetadata.setContentType(Files.probeContentType(file.toPath()));
            }
            return encryptFile(new FileInputStream(file), secretFileMetadata);
        });
    }

    public InputStream encryptFile(InputStream inputStream, SecretFileMetadata secretFileMetadata) {
        Objects.requireNonNull(inputStream);
        secretFileMetadata = secretFileMetadata == null ? new SecretFileMetadata() : secretFileMetadata;

        try {
            secretFileMetadata = secretFileMetadata.clone();

            ensureStarted();

            try (ClearableBytes filePassword = ClearableBytes.take(configuration.getRandomnessProvider().randomBytes(encryptionSuite.getKeyAndIvLength()))) {
                byte[] fileIv = configuration.getRandomnessProvider().randomBytes(encryptionSuite.getKeyAndIvLength());
                secretFileMetadata.setFilePassword(filePassword.getData());
                secretFileMetadata.setFileIv(fileIv);

                byte[] secretFileMetadataBytes = secretFileMetadata.serializeToBytes();

                return MCourierException.translate(() -> {
                    secretShipmentMetadata.addFileSecretOffset(this.allSecretFileMetadataBytes.getData().length, secretFileMetadataBytes.length);
                    this.allSecretFileMetadataBytes.appendTake(secretFileMetadataBytes);
                    InputStream is = encryptionSuite.encryptStream(filePassword.getData(), fileIv, inputStream);
                    this.inputStreams.add(is);
                    return is;
                });
            }
        } catch (Exception e) {
            try {
                inputStream.close();
            } catch (Exception ee) {
                /* silently ignore */
            }
            throw e;
        }
    }

    public EncryptedShipment finalizeShipment(byte[] password, long expiresOn) {
        ensureStarted();

        byte[] masterIv = configuration.getRandomnessProvider().randomBytes(encryptionSuite.getKeyAndIvLength());

        byte[] challengeFinalSeed = configuration.getRandomnessProvider().randomBytes(configuration.getFinalSeedLength());
        byte[] challengeTransformSeed = configuration.getRandomnessProvider().randomBytes(encryptionSuite.getTransformSeedLength());

        for (int i = 0; i < configuration.getChallengesCount(); i++) {
            byte[] challenge = configuration.getRandomnessProvider().randomBytes(encryptionSuite.getTransformSeedLength());
            byte[] response = encryptionSuite.generateChallengeResponse(password, challenge, challengeTransformSeed, challengeFinalSeed, configuration.getTransformSpec());
            hostingShipmentMetadata.addChallengeResponse(challenge, response);
        }

        byte[] encryptedData;
        byte[] encryptedDataSignature;
        try (ClearableBytes unencryptedData = new ClearableBytes()) {
            try (ClearableBytes unencryptedSecretMetadata = ClearableBytes.take(secretShipmentMetadata.serializeToBytes())) {
                unencryptedData.appendTake(Utility.encodeIntegers(unencryptedSecretMetadata.getData().length, allSecretFileMetadataBytes.getData().length));
                unencryptedData.appendTake(unencryptedSecretMetadata);
            }

            unencryptedData.appendTake(allSecretFileMetadataBytes);

            try (ClearableBytes masterKey = encryptionSuite.generateMasterKey(password, masterTransformSeed, masterFinalSeed, configuration.getTransformSpec())) {
                encryptedData = encryptionSuite.encryptData(masterKey.getData(), masterIv, unencryptedData.getData());
            }

            try (ClearableBytes signKey = encryptionSuite.generateSignKey(password, masterTransformSeed, masterFinalSeed, configuration.getTransformSpec())) {
                encryptedDataSignature = encryptionSuite.sign(signKey.getData(), encryptedData);
            }
        }

        publicShipmentMetadata
                .encryptionSuite(encryptionSuite.getId())
                .masterFinalSeed(masterFinalSeed)
                .masterTransformSeed(masterTransformSeed)
                .challengeFinalSeed(challengeFinalSeed)
                .challengeTransformSeed(challengeTransformSeed)
                .transformSpec(configuration.getTransformSpec())
                .iv(masterIv)
                .encryptedDataSignature(encryptedDataSignature);

        hostingShipmentMetadata
                .expiresOn(expiresOn);

        EncryptedShipment shipment = new EncryptedShipment();
        shipment.setPublicShipmentMetadata(publicShipmentMetadata);
        shipment.setHostingShipmentMetadata(hostingShipmentMetadata);
        shipment.setEncryptedData(encryptedData);
        this.encryptedShipment = shipment;
        return shipment;
    }

    protected void ensureNotStarted() {
        if (hasStarted) {
            throw new IllegalStateException("Shipment creation has already started.");
        }
    }

    protected void ensureStarted() {
        if (hasStarted) {
            return;
        }

        masterFinalSeed = configuration.getRandomnessProvider().randomBytes(configuration.getFinalSeedLength());
        masterTransformSeed = configuration.getRandomnessProvider().randomBytes(encryptionSuite.getTransformSeedLength());
        allSecretFileMetadataBytes = new ClearableBytes();
        inputStreams = new ArrayList<>();

        hasStarted = true;
    }

    public PublicShipmentMetadata getPublicShipmentMetadata() {
        return publicShipmentMetadata;
    }

    public void setPublicShipmentMetadata(PublicShipmentMetadata publicShipmentMetadata) {
        ensureNotStarted();
        Objects.requireNonNull(publicShipmentMetadata);
        this.publicShipmentMetadata = publicShipmentMetadata.clone();
    }

    public SecretShipmentMetadata getSecretShipmentMetadata() {
        return secretShipmentMetadata;
    }

    public void setSecretShipmentMetadata(SecretShipmentMetadata secretShipmentMetadata) {
        ensureNotStarted();
        Objects.requireNonNull(secretShipmentMetadata);
        this.secretShipmentMetadata = secretShipmentMetadata.clone();
    }

    public HostingShipmentMetadata getHostingShipmentMetadata() {
        return hostingShipmentMetadata;
    }

    public void setHostingShipmentMetadata(HostingShipmentMetadata hostingShipmentMetadata) {
        ensureNotStarted();
        Objects.requireNonNull(hostingShipmentMetadata);
        this.hostingShipmentMetadata = hostingShipmentMetadata.clone();
    }

    public EncryptedShipment getEncryptedShipment() {
        return encryptedShipment;
    }

    public List<InputStream> getInputStreams() {
        return inputStreams;
    }

    @Override
    public void close() {
        if (this.inputStreams != null && !this.inputStreams.isEmpty()) {
            for (int i=0; i<this.inputStreams.size(); i++) {
                if (this.inputStreams.get(i) != null) {
                    try {
                        this.inputStreams.get(i).close();
                        this.inputStreams.set(i, null);
                    } catch (Exception e) {
                        /* silently ignore */
                    }
                }
            }
            this.inputStreams = null;
        }
    }
}
