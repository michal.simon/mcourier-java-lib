package eu.bindworks.mcourier.crypto;

public class ShipmentEncryptorConfiguration {
    private EncryptionSuite encryptionSuite = EncryptionSuite.VERSION_1;
    private RandomnessProvider randomnessProvider = new SecureRandomRandomnessProvider();
    private int finalSeedLength = 32;
    private String transformSpec = "m32768,i3,p1";
    private int challengesCount = 4;

    public RandomnessProvider getRandomnessProvider() {
        return randomnessProvider;
    }

    public void setRandomnessProvider(RandomnessProvider randomnessProvider) {
        this.randomnessProvider = randomnessProvider;
    }

    public EncryptionSuite getEncryptionSuite() {
        return encryptionSuite;
    }

    public void setEncryptionSuite(EncryptionSuite encryptionSuite) {
        this.encryptionSuite = encryptionSuite;
    }

    public int getFinalSeedLength() {
        return finalSeedLength;
    }

    public void setFinalSeedLength(int finalSeedLength) {
        this.finalSeedLength = finalSeedLength;
    }

    public String getTransformSpec() {
        return transformSpec;
    }

    public void setTransformSpec(String transformSpec) {
        this.transformSpec = transformSpec;
    }

    public void setArgon2TransformSpec(long memoryInKb, int iterations, int parallelism) {
        this.transformSpec = String.format("m%d,i%d,p%d", memoryInKb, iterations, parallelism);
    }

    public int getChallengesCount() {
        return challengesCount;
    }

    public void setChallengesCount(int challengesCount) {
        this.challengesCount = challengesCount;
    }
}
