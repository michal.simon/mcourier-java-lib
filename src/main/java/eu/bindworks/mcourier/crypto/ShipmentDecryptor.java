package eu.bindworks.mcourier.crypto;

import eu.bindworks.mcourier.DecryptedShipment;
import eu.bindworks.mcourier.EncryptedShipment;
import eu.bindworks.mcourier.HostingShipmentMetadata;
import eu.bindworks.mcourier.MCourierException;
import eu.bindworks.mcourier.PublicShipmentMetadata;
import eu.bindworks.mcourier.SecretFileMetadata;
import eu.bindworks.mcourier.SecretShipmentMetadata;
import eu.bindworks.mcourier.Utility;

import java.util.Arrays;

public class ShipmentDecryptor {
    public static final int MINIMUM_SEED_LENGTH = 16;
    public static final int MAXIMUM_SEED_LENGTH = 128;
    public static final int MINIMUM_CHALLENGES_COUNT = 4;
    public static final int MAXIMUM_CHALLENGES_COUNT = 16;

    public boolean validateShipment(EncryptedShipment encryptedShipment) {
        if (!encryptedShipment.validateSyntax()) {
            return false;
        }

        PublicShipmentMetadata publicShipmentMetadata = encryptedShipment.getPublicShipmentMetadata();
        HostingShipmentMetadata hostingShipmentMetadata = encryptedShipment.getHostingShipmentMetadata();

        if (!EncryptionSuite.existsForId(publicShipmentMetadata.getEncryptionSuite())
                || publicShipmentMetadata.getMasterFinalSeed().length < MINIMUM_SEED_LENGTH
                || publicShipmentMetadata.getMasterFinalSeed().length > MAXIMUM_SEED_LENGTH
                || publicShipmentMetadata.getChallengeFinalSeed().length < MINIMUM_SEED_LENGTH
                || publicShipmentMetadata.getChallengeFinalSeed().length > MAXIMUM_SEED_LENGTH) {
            return false;
        }

        EncryptionSuite suite = EncryptionSuite.forId(publicShipmentMetadata.getEncryptionSuite());

        if (publicShipmentMetadata.getIv().length != suite.getKeyAndIvLength()
            || publicShipmentMetadata.getMasterTransformSeed().length != suite.getTransformSeedLength()
            || publicShipmentMetadata.getChallengeTransformSeed().length != suite.getTransformSeedLength()
            || publicShipmentMetadata.getEncryptedDataSignature().length != suite.getSignatureLength()) {
            return false;
        }

        int challengesCount = hostingShipmentMetadata.getChallengesCount();
        if (challengesCount < MINIMUM_CHALLENGES_COUNT
            || challengesCount > MAXIMUM_CHALLENGES_COUNT) {
            return false;
        }

        return true;
    }

    public ClearableBytes normalizePassword(EncryptedShipment shipment, byte[] password) {
        String passwordNormalization = shipment.getPublicShipmentMetadata().getPasswordNormalization();
        ClearableBytes bytes = ClearableBytes.copy(password);
        PasswordNormalizerBuilder
                .forSequence(passwordNormalization == null ? PasswordGenerator.NORMALIZER_SEQUENCE : passwordNormalization)
                .normalize(bytes);
        return bytes;
    }

    public boolean isShipmentPasswordValid(EncryptedShipment shipment, byte[] password) {
        return isChallengeValid(shipment, password, 0);
    }

    public boolean isChallengeValid(EncryptedShipment shipment, byte[] password, int challengeIndex) {
        if (challengeIndex < 0 || challengeIndex >= shipment.getHostingShipmentMetadata().getChallengesCount()) {
            return false;
        }

        try (ClearableBytes normalizedPassword = normalizePassword(shipment, password)) {
            EncryptionSuite encryptionSuite = EncryptionSuite.forId(shipment.getPublicShipmentMetadata().getEncryptionSuite());
            byte[] challenge = shipment.getHostingShipmentMetadata().getChallenge(challengeIndex);
            byte[] expectedResponse = shipment.getHostingShipmentMetadata().getChallengeResponse(challengeIndex);
            byte[] response = encryptionSuite.generateChallengeResponse(
                    normalizedPassword.getData(),
                    challenge,
                    shipment.getPublicShipmentMetadata().getChallengeTransformSeed(),
                    shipment.getPublicShipmentMetadata().getChallengeFinalSeed(),
                    shipment.getPublicShipmentMetadata().getTransformSpec());

            return Arrays.equals(response, expectedResponse);
        }
    }

    public boolean isShipmentSignatureValid(EncryptedShipment shipment, byte[] password) {
        EncryptionSuite encryptionSuite = EncryptionSuite.forId(shipment.getPublicShipmentMetadata().getEncryptionSuite());

        try (ClearableBytes normalizedPassword = normalizePassword(shipment, password)) {
            try (ClearableBytes signKey = encryptionSuite.generateSignKey(
                    normalizedPassword.getData(),
                    shipment.getPublicShipmentMetadata().getMasterTransformSeed(),
                    shipment.getPublicShipmentMetadata().getMasterFinalSeed(),
                    shipment.getPublicShipmentMetadata().getTransformSpec())) {
                byte[] signature = encryptionSuite.sign(signKey.getData(), shipment.getEncryptedData());
                if (!Arrays.equals(signature, shipment.getPublicShipmentMetadata().getEncryptedDataSignature())) {
                    return false;
                }
            }
        }

        return true;
    }

    public DecryptedShipment decryptShipment(EncryptedShipment encryptedShipment, byte[] password) {
        PublicShipmentMetadata publicShipmentMetadata = encryptedShipment.getPublicShipmentMetadata();
        HostingShipmentMetadata hostingShipmentMetadata = encryptedShipment.getHostingShipmentMetadata();

        int encryptionSuiteId = publicShipmentMetadata.getEncryptionSuite();
        EncryptionSuite encryptionSuite = EncryptionSuite.forId(encryptionSuiteId);

        try (ClearableBytes normalizedPassword = normalizePassword(encryptedShipment, password)) {
            byte[] decryptedData;
            try (ClearableBytes masterKey = encryptionSuite.generateMasterKey(
                    normalizedPassword.getData(),
                    publicShipmentMetadata.getMasterTransformSeed(),
                    publicShipmentMetadata.getMasterFinalSeed(),
                    publicShipmentMetadata.getTransformSpec())) {
                decryptedData = encryptionSuite.decryptData(masterKey.getData(), publicShipmentMetadata.getIv(), encryptedShipment.getEncryptedData());
            }

            int[] lengths = Utility.decodeIntegers(Arrays.copyOfRange(decryptedData, 0, 8));
            int secretShipmentMetadataLength = lengths[0];
            int secretFileMetadataLength = lengths[1];
            if (secretShipmentMetadataLength + secretFileMetadataLength + 8 != decryptedData.length) {
                throw new MCourierException.EncryptionDecryptionException("Lengths in the header do not match");
            }

            SecretShipmentMetadata secretShipmentMetadata = new SecretShipmentMetadata();
            secretShipmentMetadata.deserializeFromBytes(decryptedData, 8, secretShipmentMetadataLength);

            int fileCount = secretShipmentMetadata.getFilesCount();
            SecretFileMetadata[] secretFileMetadata = new SecretFileMetadata[fileCount];
            for (int i = 0; i < fileCount; i++) {
                SecretFileMetadata fmd = new SecretFileMetadata();
                fmd.deserializeFromBytes(decryptedData, 8 + secretShipmentMetadataLength + secretShipmentMetadata.getFileSecretOffset(i), secretShipmentMetadata.getFileSecretLength(i));
                secretFileMetadata[i] = fmd;
            }

            return new DecryptedShipment(encryptionSuite,
                    publicShipmentMetadata,
                    hostingShipmentMetadata,
                    secretShipmentMetadata,
                    secretFileMetadata);
        }
    }
}
