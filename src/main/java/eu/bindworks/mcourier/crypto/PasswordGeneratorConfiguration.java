package eu.bindworks.mcourier.crypto;

public class PasswordGeneratorConfiguration {
    private String passwordDigestionAlgorithm = "SHA-256";
    private int passwordLength = 8;
    private int digestRounds = 32;
    private RandomnessProvider randomnessProvider = new SecureRandomRandomnessProvider();
    private PasswordNormalizer passwordNormalizer = PasswordNormalizerBuilder.forSequence(PasswordNormalizerBuilder.DEFAULT_NORMALIZER_SEQUENCE);

    public RandomnessProvider getRandomnessProvider() {
        return randomnessProvider;
    }

    public void setRandomnessProvider(RandomnessProvider randomnessProvider) {
        this.randomnessProvider = randomnessProvider;
    }

    public String getPasswordDigestionAlgorithm() {
        return passwordDigestionAlgorithm;
    }

    public void setPasswordDigestionAlgorithm(String passwordDigestionAlgorithm) {
        this.passwordDigestionAlgorithm = passwordDigestionAlgorithm;
    }

    public int getPasswordLength() {
        return passwordLength;
    }

    public void setPasswordLength(int passwordLength) {
        this.passwordLength = passwordLength;
    }

    public int getDigestRounds() {
        return digestRounds;
    }

    public void setDigestRounds(int digestRounds) {
        this.digestRounds = digestRounds;
    }

    public PasswordNormalizer getPasswordNormalizer() {
        return passwordNormalizer;
    }

    public void setPasswordNormalizer(PasswordNormalizer passwordNormalizer) {
        this.passwordNormalizer = passwordNormalizer;
    }
}
