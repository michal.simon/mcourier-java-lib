package eu.bindworks.mcourier.crypto;

import java.io.Closeable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class CloseableCollection implements Closeable {
    private List<Closeable> closeables = new ArrayList<>();

    public <T extends Closeable> T add(T closeable) {
        closeables.add(closeable);
        return closeable;
    }

    @Override
    public void close() {
        List<Closeable> toClose = closeables;
        closeables = new ArrayList<>();
        Collections.reverse(toClose);

        for (Closeable closeable: toClose) {
            try {
                closeable.close();
            } catch (Exception e) {
                /* silently ignore */
            }
        }
    }
}
