package eu.bindworks.mcourier.crypto;

public interface PasswordNormalizer {
    String getSequence();
    void normalize(ClearableBytes password);
}
