package eu.bindworks.mcourier.crypto;

import com.kosprov.jargon2.api.Jargon2;
import eu.bindworks.mcourier.MCourierException;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.bouncycastle.jsse.provider.BouncyCastleJsseProvider;

import javax.crypto.Cipher;
import javax.crypto.CipherInputStream;
import javax.crypto.Mac;
import javax.crypto.spec.GCMParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.security.Key;
import java.security.MessageDigest;
import java.security.Provider;
import java.util.Map;
import java.util.Objects;
import java.util.zip.DeflaterInputStream;
import java.util.zip.InflaterInputStream;

public abstract class EncryptionSuite {
    public static final Provider BOUNCY_CASTLE_PROVIDER = new BouncyCastleProvider();
    public static final Provider BOUNCY_CASTLE_JSSE_PROVIDER = new BouncyCastleJsseProvider();

    protected static final byte[] PURPOSE_MASTER_KEY = "master_key".getBytes(StandardCharsets.UTF_8);
    protected static final byte[] PURPOSE_SIGN_KEY = "sign_key".getBytes(StandardCharsets.UTF_8);

    public static final int VERSION_1_ID = 0x12340001;
    public static final EncryptionSuite VERSION_1 = new Version1EncryptionSuite(VERSION_1_ID);
    private static final Map<Integer, EncryptionSuite> SUITES = Map.of(VERSION_1.id, VERSION_1);

    private int id;

    public static EncryptionSuite forId(int id) {
        return SUITES.get(id);
    }

    public static boolean existsForId(int id) {
        return SUITES.containsKey(id);
    }

    public int getId() {
        return id;
    }

    public EncryptionSuite(int id) {
        this.id = id;
    }

    public ClearableBytes generateMasterKey(byte[] password, byte[] transformSeed, byte[] finalSeed, String transformSpec) {
        return generateDerivedEncryptionKey(password, PURPOSE_MASTER_KEY, transformSeed, finalSeed, transformSpec);
    }

    public ClearableBytes generateSignKey(byte[] password, byte[] transformSeed, byte[] finalSeed, String transformSpec) {
        return generateDerivedSignKey(password, PURPOSE_SIGN_KEY, transformSeed, finalSeed, transformSpec);
    }

    public byte[] generateChallengeResponse(byte[] password, byte[] challenge, byte[] transformSeed, byte[] finalSeed, String transformSpec) {
        return generateDerivedEncryptionKey(password, challenge, transformSeed, finalSeed, transformSpec).getData();
    }

    public abstract int getTransformSeedLength();
    public abstract int getKeyAndIvLength();
    public abstract int getSignatureLength();

    protected abstract ClearableBytes generateDerivedEncryptionKey(byte[] password, byte[] purpose, byte[] transformSeed, byte[] finalSeed, String transformSpec);
    protected abstract ClearableBytes generateDerivedSignKey(byte[] password, byte[] purpose, byte[] transformSeed, byte[] finalSeed, String transformSpec);

    public abstract InputStream encryptStream(byte[] key, byte[] iv, InputStream is);
    public abstract InputStream decryptStream(byte[] key, byte[] iv, InputStream is);

    public abstract byte[] sign(byte[] key, byte[] data);

    public byte[] encryptData(byte[] key, byte[] iv, byte[] data) {
        Objects.requireNonNull(key);
        Objects.requireNonNull(data);

        return MCourierException.translate(() ->
                encryptStream(key, iv, new ByteArrayInputStream(data)).readAllBytes());
    }

    public byte[] decryptData(byte[] key, byte[] iv, byte[] data) {
        Objects.requireNonNull(key);
        Objects.requireNonNull(data);

        return MCourierException.translate(() ->
                decryptStream(key, iv, new ByteArrayInputStream(data)).readAllBytes());
    }

    public static class Version1EncryptionSuite extends EncryptionSuite {
        public Version1EncryptionSuite(int id) {
            super(id);
        }

        @Override
        public int getTransformSeedLength() {
            return 32;
        }

        @Override
        public int getKeyAndIvLength() {
            return 32;
        }

        @Override
        public int getSignatureLength() {
            return 64;
        }

        @Override
        protected ClearableBytes generateDerivedEncryptionKey(byte[] password, byte[] purpose, byte[] transformSeed, byte[] finalSeed, String transformSpec) {
            return generateDerivedKey(password, purpose, transformSeed, finalSeed, transformSpec);
        }

        @Override
        protected ClearableBytes generateDerivedSignKey(byte[] password, byte[] purpose, byte[] transformSeed, byte[] finalSeed, String transformSpec) {
            return generateDerivedKey(password, purpose, transformSeed, finalSeed, transformSpec);
        }

        protected ClearableBytes generateDerivedKey(byte[] password, byte[] purpose, byte[] transformSeed, byte[] finalSeed, String transformSpec) {
            Objects.requireNonNull(password);
            Objects.requireNonNull(purpose);
            Objects.requireNonNull(transformSeed);
            Objects.requireNonNull(finalSeed);
            Objects.requireNonNull(transformSpec);
            if (transformSeed.length != getTransformSeedLength()) {
                throw new IllegalArgumentException("Transform seed must be of " + getTransformSeedLength() + " length");
            }

            MessageDigest compositeKeyDigest = newPasswordDigest();
            compositeKeyDigest.update(newPasswordDigest().digest(password));
            compositeKeyDigest.update(newPasswordDigest().digest(purpose));

            Jargon2.Hasher hasher = parseTransformationSpec(transformSpec)
                    .saltLength(getTransformSeedLength())
                    .hashLength(getKeyAndIvLength());

            try (ClearableBytes compositeKey = ClearableBytes.take(compositeKeyDigest.digest())) {
                try (ClearableBytes transformedKey = ClearableBytes.take(hasher
                        .password(compositeKey.getData())
                        .salt(transformSeed)
                        .rawHash())) {
                    compositeKey.clear();
                    try (ClearableBytes finalRoundData = new ClearableBytes()) {
                        finalRoundData.appendCopy(finalSeed);
                        finalRoundData.appendTake(transformedKey);
                        return ClearableBytes.take(newPasswordDigest().digest(finalRoundData.getData()));
                    }
                }
            }
        }

        @Override
        public InputStream encryptStream(byte[] key, byte[] ivData, InputStream is) {
            Cipher c = newDataCipher();
            Key k = new SecretKeySpec(key, "AES");
            GCMParameterSpec gcm = new GCMParameterSpec(128, ivData);
            MCourierException.translate(() -> c.init(Cipher.ENCRYPT_MODE, k, gcm));
            return new CipherInputStream(new DeflaterInputStream(is), c);
        }

        @Override
        public InputStream decryptStream(byte[] key, byte[] ivData, InputStream is) {
            Cipher c = newDataCipher();
            Key k = new SecretKeySpec(key, "AES");
            GCMParameterSpec gcm = new GCMParameterSpec(128, ivData);
            MCourierException.translate(() -> c.init(Cipher.DECRYPT_MODE, k, gcm));
            return new InflaterInputStream(new CipherInputStream(is, c));
        }

        @Override
        public byte[] sign(byte[] key, byte[] data) {
            Mac mac = newMac();
            SecretKeySpec keySpec = new SecretKeySpec(key, "HmacSHA512");
            return MCourierException.translate(() -> {
                mac.init(keySpec);
                return mac.doFinal(data);
            });
        }

        private static Cipher newDataCipher() {
            return MCourierException.translate(() -> Cipher.getInstance("AES/GCM/NoPadding", BOUNCY_CASTLE_PROVIDER));
        }

        private static MessageDigest newPasswordDigest() {
            return MCourierException.translate(() -> MessageDigest.getInstance("SHA-256", BOUNCY_CASTLE_PROVIDER));
        }

        private static Mac newMac() {
            return MCourierException.translate(() -> Mac.getInstance("HmacSHA512", BOUNCY_CASTLE_PROVIDER));
        }

        public static Jargon2.Hasher parseTransformationSpec(String transformationSpec) {
            Jargon2.Hasher builder = Jargon2.jargon2Hasher()
                    .type(Jargon2.Type.ARGON2id)
                    .version(Jargon2.Version.V13);
            for (String part: transformationSpec.split(",")) {
                int argument = Integer.parseInt(part.substring(1));
                switch (part.charAt(0)) {
                    case 'm': builder = builder.memoryCost(argument); break;
                    case 'i': builder = builder.timeCost(argument); break;
                    case 'p': builder = builder.parallelism(argument); break;
                    default: throw new IllegalArgumentException("Unknown transformation argument " + part.charAt(0));
                }
            }

            return builder;
        }
    }
}
