package eu.bindworks.mcourier.crypto;

public interface RandomnessProvider {
    byte[] randomBytes(int count);
}
