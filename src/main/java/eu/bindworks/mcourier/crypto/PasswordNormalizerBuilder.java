package eu.bindworks.mcourier.crypto;

import java.nio.charset.StandardCharsets;
import java.text.Normalizer;
import java.util.Arrays;

public class PasswordNormalizerBuilder {
    public static final String NONMIXABLE_CHARACTERS = "0123456789ABCDEFGHJKMNPRTUVWXYZ";
    public static final String NORMALIZER_NAME_UPPERCASE = "uppercase";
    public static final String NORMALIZER_NAME_LOWERCASE = "lowercase";
    public static final String NORMALIZER_NAME_REMOVE_DIACRITICS = "remove_diacritics";
    public static final String NORMALIZER_NAME_NONMIXABLE = "nonmixable";

    public static final String DEFAULT_NORMALIZER_SEQUENCE = NORMALIZER_NAME_UPPERCASE + " " + NORMALIZER_NAME_REMOVE_DIACRITICS + " " + NORMALIZER_NAME_NONMIXABLE;

    public static final PasswordNormalizer NORMALIZER_UPPERCASE = new UppercaseNormalizer();
    public static final PasswordNormalizer NORMALIZER_LOWERCASE = new LowercaseNormalizer();
    public static final PasswordNormalizer NORMALIZER_REMOVE_DIACRITICS = new RemoveDiacriticsNormalizer();
    public static final PasswordNormalizer NORMALIZER_NONMIXABLE = new NonmixableNormalizer();

    private PasswordNormalizerBuilder() {
    }

    public static PasswordNormalizer forSequence(String sequence) {
        String[] names = sequence.split("\\s+");
        PasswordNormalizer[] steps = Arrays.stream(names).map(PasswordNormalizerBuilder::forName).toArray(PasswordNormalizer[]::new);
        return new CombinedNormalizer(sequence, steps);
    }

    public static PasswordNormalizer forName(String name) {
        switch (name) {
            case NORMALIZER_NAME_UPPERCASE:
                return NORMALIZER_UPPERCASE;
            case NORMALIZER_NAME_LOWERCASE:
                return NORMALIZER_LOWERCASE;
            case NORMALIZER_NAME_REMOVE_DIACRITICS:
                return NORMALIZER_REMOVE_DIACRITICS;
            case NORMALIZER_NAME_NONMIXABLE:
                return NORMALIZER_NONMIXABLE;
            default:
                throw new IllegalArgumentException(name);
        }
    }

    private static class CombinedNormalizer implements PasswordNormalizer {
        private final String sequence;
        private final PasswordNormalizer[] steps;

        public CombinedNormalizer(String sequence, PasswordNormalizer[] steps) {
            this.sequence = sequence;
            this.steps = steps;
        }

        @Override
        public String getSequence() {
            return sequence;
        }

        @Override
        public void normalize(ClearableBytes password) {
            for (PasswordNormalizer step: steps) {
                step.normalize(password);
            }
        }
    }

    private static class UppercaseNormalizer implements PasswordNormalizer {
        @Override
        public String getSequence() {
            return NORMALIZER_NAME_UPPERCASE;
        }

        @Override
        public void normalize(ClearableBytes password) {
            password.replaceTake(new String(password.getData(), StandardCharsets.UTF_8).toUpperCase().getBytes(StandardCharsets.UTF_8));
        }
    }

    private static class LowercaseNormalizer implements PasswordNormalizer {
        @Override
        public String getSequence() {
            return NORMALIZER_NAME_LOWERCASE;
        }

        @Override
        public void normalize(ClearableBytes password) {
            password.replaceTake(new String(password.getData(), StandardCharsets.UTF_8).toLowerCase().getBytes(StandardCharsets.UTF_8));
        }
    }

    private static class RemoveDiacriticsNormalizer implements PasswordNormalizer {
        @Override
        public String getSequence() {
            return NORMALIZER_NAME_REMOVE_DIACRITICS;
        }

        @Override
        public void normalize(ClearableBytes password) {
            String tmp = new String(password.getData(), StandardCharsets.UTF_8);
            tmp = Normalizer.normalize(tmp, Normalizer.Form.NFD);
            tmp = tmp.replaceAll("\\p{M}", "");
            password.replaceTake(tmp.getBytes(StandardCharsets.UTF_8));
        }
    }

    private static class NonmixableNormalizer implements PasswordNormalizer {
        @Override
        public String getSequence() {
            return NORMALIZER_NAME_NONMIXABLE;
        }

        @Override
        public void normalize(ClearableBytes password) {
            try (ClearableBytes newPassword = new ClearableBytes(password.getData().length)) {
                int index = 0;
                for(byte c: password.getData()) {
                    switch (c) {
                        case 'I':
                        case 'i':
                        case 'L':
                        case 'l':
                            newPassword.getData()[index++] = '1';
                            break;
                        case 'S':
                        case 's':
                            newPassword.getData()[index++] = '5';
                            break;
                        case 'O':
                        case 'o':
                        case 'Q':
                        case 'q':
                            newPassword.getData()[index++] = '0';
                            break;
                        default:
                            if (('0' <= c && c <= '9') || ('a' <= c && c <= 'z') || ('A' <= c && c <= 'Z')) {
                                newPassword.getData()[index++] = c;
                            }
                            break;
                    }
                }

                try (ClearableBytes cutPassword = new ClearableBytes(index)) {
                    System.arraycopy(newPassword.getData(), 0, cutPassword.getData(), 0, cutPassword.getData().length);
                    password.replaceCopy(cutPassword);
                }
            }
        }
    }
}
