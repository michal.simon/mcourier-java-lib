package eu.bindworks.mcourier.crypto;

import java.io.Closeable;
import java.util.Arrays;

public class ClearableBytes implements AutoCloseable, Closeable {
    private static final byte[] EMPTY_DATA = new byte[0];
    private byte[] data;

    public ClearableBytes() {
        this.data = EMPTY_DATA;
    }

    public ClearableBytes(int i) {
        this.data = new byte[i];
    }

    public byte[] getData() {
        return data;
    }

    public static ClearableBytes copy(byte[] buffer) {
        ClearableBytes cb = new ClearableBytes();
        cb.replaceCopy(buffer);
        return cb;
    }

    public static ClearableBytes take(byte[] buffer) {
        ClearableBytes cb = new ClearableBytes();
        cb.replaceTake(buffer);
        return cb;
    }

    public void appendCopy(byte[] buffer) {
        byte[] oldData = data;
        try {
            byte[] newData = new byte[data.length + buffer.length];
            System.arraycopy(data, 0, newData, 0, data.length);
            System.arraycopy(buffer, 0, newData, data.length, buffer.length);
            data = newData;
        } finally {
            Arrays.fill(oldData, (byte) 0);
        }
    }

    public void appendCopy(ClearableBytes bytes) {
        appendCopy(bytes.getData());
    }

    public void appendTake(byte[] buffer) {
        try {
            appendCopy(buffer);
        } finally {
            clear(buffer);
        }
    }

    public void appendTake(ClearableBytes bytes) {
        try {
            appendCopy(bytes);
        } finally {
            bytes.clear();
        }
    }

    public void replaceCopy(byte[] buffer) {
        byte[] oldData = data;
        try {
            byte[] newData = new byte[buffer.length];
            System.arraycopy(buffer, 0, newData, 0, buffer.length);
            data = newData;
        } finally {
            Arrays.fill(oldData, (byte) 0);
        }
    }

    public void replaceCopy(ClearableBytes bytes) {
        replaceCopy(bytes.getData());
    }

    public void replaceTake(byte[] buffer) {
        try {
            replaceCopy(buffer);
        } finally {
            clear(buffer);
        }
    }

    public void replaceTake(ClearableBytes bytes) {
        try {
            replaceCopy(bytes);
        } finally {
            bytes.clear();
        }
    }

    public void clear() {
        Arrays.fill(data, (byte) 0);
        data = new byte[0];
    }

    public static void clear(byte[] buffer) {
        if (buffer == null) {
            return;
        }
        Arrays.fill(buffer, (byte) 0);
    }

    @Override
    public void close() {
        clear();
    }
}
