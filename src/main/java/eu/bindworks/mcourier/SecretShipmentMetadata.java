package eu.bindworks.mcourier;

public class SecretShipmentMetadata extends Metadata<SecretShipmentMetadata> {
    public static final int SIGNATURE = Utility.fourLetterCode("mCss");
    public static final String KEY_FILE_SECRET_OFFSET_PREFIX = "_fso";
    public static final String KEY_FILE_SECRET_LENGTH_PREFIX = "_fsl";
    public static final String KEY_FILES_COUNT = "_fc";

    public static final String KEY_DESCRIPTION = "description";
    public static final String KEY_ICON_URL = "iconUrl";

    @Override
    protected SecretShipmentMetadata construct() {
        return new SecretShipmentMetadata();
    }

    @Override
    public boolean validateSyntax() {
        if (!super.validateSyntax() || !isInt(KEY_FILES_COUNT)) {
            return false;
        }

        int count = getFilesCount();
        for (int i=0; i<count; i++) {
            if (!isInt(KEY_FILE_SECRET_OFFSET_PREFIX + i)
                || !isInt(KEY_FILE_SECRET_LENGTH_PREFIX + i)) {
                return false;
            }
        }

        return true;
    }

    @Override
    protected int getSerializationSignature() {
        return SIGNATURE;
    }

    public int getFilesCount() {
        return getInt(KEY_FILES_COUNT);
    }

    public int getFileSecretOffset(int i) {
        return getInt(KEY_FILE_SECRET_OFFSET_PREFIX + i);
    }

    public int getFileSecretLength(int i) {
        return getInt(KEY_FILE_SECRET_LENGTH_PREFIX + i);
    }

    public SecretShipmentMetadata addFileSecretOffset(int offset, int length) {
        Integer fileCount = getInt(KEY_FILES_COUNT);
        if (fileCount == null) {
            fileCount = 0;
        }
        setInt(KEY_FILE_SECRET_OFFSET_PREFIX + fileCount.toString(), offset);
        setInt(KEY_FILE_SECRET_LENGTH_PREFIX + fileCount.toString(), length);
        fileCount += 1;
        setInt(KEY_FILES_COUNT, fileCount);
        return this;
    }

    public String getDescription() {
        return get(KEY_DESCRIPTION);
    }

    public void setDescription(String description) {
        set(KEY_DESCRIPTION, description);
    }

    public SecretShipmentMetadata description(String description) {
        return set(KEY_DESCRIPTION, description);
    }

    public String getIconUrl() {
        return get(KEY_ICON_URL);
    }

    public void setIconUrl(String iconUrl) {
        set(KEY_ICON_URL, iconUrl);
    }

    public SecretShipmentMetadata iconUrl(String iconUrl) {
        return set(KEY_ICON_URL, iconUrl);
    }

}
