package eu.bindworks.mcourier;

public class SecretFileMetadata extends Metadata<SecretFileMetadata> {
    public static final int SIGNATURE = Utility.fourLetterCode("mCsf");

    public static final String KEY_FILE_PASSWORD = "_p";
    public static final String KEY_FILE_IV = "_iv";

    public static final String KEY_CONTENT_TYPE = "contentType";
    public static final String KEY_FILENAME = "fileName";
    public static final String KEY_DESCRIPTION = "description";
    public static final String KEY_ICON_URL = "iconUrl";

    @Override
    protected SecretFileMetadata construct() {
        return new SecretFileMetadata();
    }

    @Override
    public boolean validateSyntax() {
        return super.validateSyntax()
                && isBytes(KEY_FILE_PASSWORD)
                && isString(KEY_CONTENT_TYPE)
                && isString(KEY_FILENAME);
    }

    @Override
    protected int getSerializationSignature() {
        return SIGNATURE;
    }

    public byte[] getFilePassword() {
        return getBytes(KEY_FILE_PASSWORD);
    }

    public void setFilePassword(byte[] value) {
        setBytes(KEY_FILE_PASSWORD, value);
    }

    public SecretFileMetadata filePassword(byte[] value) {
        return setBytes(KEY_FILE_PASSWORD, value);
    }

    public byte[] getFileIv() {
        return getBytes(KEY_FILE_IV);
    }

    public void setFileIv(byte[] value) {
        setBytes(KEY_FILE_IV, value);
    }

    public SecretFileMetadata fileIv(byte[] value) {
        return setBytes(KEY_FILE_IV, value);
    }

    public String getFilename() {
        return get(KEY_FILENAME);
    }

    public void setFilename(String filename) {
        set(KEY_FILENAME, filename);
    }

    public SecretFileMetadata filename(String filename) {
        return set(KEY_FILENAME, filename);
    }

    public String getDescription() {
        return get(KEY_DESCRIPTION);
    }

    public void setDescription(String description) {
        set(KEY_DESCRIPTION, description);
    }

    public SecretFileMetadata description(String description) {
        return set(KEY_DESCRIPTION, description);
    }

    public String getContentType() {
        return get(KEY_CONTENT_TYPE);
    }

    public void setContentType(String contentType) {
        set(KEY_CONTENT_TYPE, contentType);
    }

    public SecretFileMetadata contentType(String contentType) {
        return set(KEY_CONTENT_TYPE, contentType);
    }

    public String getIconUrl() {
        return get(KEY_ICON_URL);
    }

    public void setIconUrl(String iconUrl) {
        set(KEY_ICON_URL, iconUrl);
    }

    public SecretFileMetadata iconUrl(String iconUrl) {
        return set(KEY_ICON_URL, iconUrl);
    }
}
