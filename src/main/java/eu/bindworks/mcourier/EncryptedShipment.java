package eu.bindworks.mcourier;

import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;
import java.util.Base64;

public class EncryptedShipment implements BytesAndJsonSerializable {
    public static final int SIGNATURE = Utility.fourLetterCode("mCes");

    private PublicShipmentMetadata publicShipmentMetadata = new PublicShipmentMetadata();
    private HostingShipmentMetadata hostingShipmentMetadata = new HostingShipmentMetadata();
    private byte[] encryptedData;

    public boolean validateSyntax() {
        return publicShipmentMetadata != null
                && hostingShipmentMetadata != null
                && encryptedData != null
                && publicShipmentMetadata.validateSyntax()
                && hostingShipmentMetadata.validateSyntax();
    }

    public PublicShipmentMetadata getPublicShipmentMetadata() {
        return publicShipmentMetadata;
    }

    public void setPublicShipmentMetadata(PublicShipmentMetadata publicShipmentMetadata) {
        this.publicShipmentMetadata = publicShipmentMetadata;
    }

    public HostingShipmentMetadata getHostingShipmentMetadata() {
        return hostingShipmentMetadata;
    }

    public void setHostingShipmentMetadata(HostingShipmentMetadata hostingShipmentMetadata) {
        this.hostingShipmentMetadata = hostingShipmentMetadata;
    }

    public byte[] getEncryptedData() {
        return encryptedData;
    }

    public void setEncryptedData(byte[] encryptedData) {
        this.encryptedData = encryptedData;
    }

    @Override
    public byte[] serializeToBytes() {
        throw new UnsupportedOperationException();
    }

    @Override
    public void deserializeFromBytes(byte[] bytes, int offset, int length) {
        throw new UnsupportedOperationException();
    }

    @Override
    public JsonObject serializeToJsonObject() {
        JsonObjectBuilder objectBuilder = Json.createObjectBuilder();
        objectBuilder.add("_", SIGNATURE);
        objectBuilder.add("_p", publicShipmentMetadata.serializeToJsonObject());
        objectBuilder.add("_h", hostingShipmentMetadata.serializeToJsonObject());
        objectBuilder.add("_e", Base64.getEncoder().encodeToString(encryptedData));
        return objectBuilder.build();
    }

    @Override
    public void deserializeFromJsonObject(JsonObject jsonObject) {
        if (!jsonObject.containsKey("_") || jsonObject.getInt("_") != SIGNATURE) {
            throw new MCourierException.EncryptionDecryptionException("Metadata signature expected " + SIGNATURE + " but got " + jsonObject.get("$"));
        }

        publicShipmentMetadata.deserializeFromJsonObject(jsonObject.getJsonObject("_p"));
        hostingShipmentMetadata.deserializeFromJsonObject(jsonObject.getJsonObject("_h"));
        encryptedData = Base64.getDecoder().decode(jsonObject.getString("_e"));
    }
}
