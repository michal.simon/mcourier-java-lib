package eu.bindworks.mcourier;

public class PublicShipmentMetadata extends Metadata<PublicShipmentMetadata> {
    public static final int SIGNATURE = Utility.fourLetterCode("mCps");

    public static final String KEY_ENCRYPTION_SUITE = "_es";
    public static final String KEY_MASTER_TRANSFORM_SEED = "_mts";
    public static final String KEY_MASTER_FINAL_SEED = "_fs";
    public static final String KEY_CHALLENGE_TRANSFORM_SEED = "_chts";
    public static final String KEY_CHALLENGE_FINAL_SEED = "_chfs";
    public static final String KEY_TRANSFORM_SPEC = "_trs";
    public static final String KEY_IV = "_iv";
    public static final String KEY_ENCRYPTED_DATA_SIGNATURE = "_sds";

    public static final String KEY_DESCRIPTION = "description";
    public static final String KEY_ICON_URL = "iconUrl";
    public static final String KEY_PASSWORD_NORMALIZATION = "pns";
    public static final String KEY_PASSWORD_MASK = "pm";

    @Override
    protected PublicShipmentMetadata construct() {
        return new PublicShipmentMetadata();
    }

    @Override
    public boolean validateSyntax() {
        return super.validateSyntax()
                && isInt(KEY_ENCRYPTION_SUITE)
                && isBytes(KEY_MASTER_FINAL_SEED)
                && isBytes(KEY_MASTER_TRANSFORM_SEED)
                && isBytes(KEY_CHALLENGE_TRANSFORM_SEED)
                && isBytes(KEY_CHALLENGE_FINAL_SEED)
                && isString(KEY_TRANSFORM_SPEC)
                && isBytes(KEY_IV)
                && isBytes(KEY_ENCRYPTED_DATA_SIGNATURE);
    }

    @Override
    protected int getSerializationSignature() {
        return SIGNATURE;
    }

    public Integer getEncryptionSuite() {
        return getInt(KEY_ENCRYPTION_SUITE);
    }

    public void setEncryptionSuite(Integer suite) {
        setInt(KEY_ENCRYPTION_SUITE, suite);
    }

    public PublicShipmentMetadata encryptionSuite(Integer suite) {
        return setInt(KEY_ENCRYPTION_SUITE, suite);
    }

    public String getTransformSpec() {
        return get(KEY_TRANSFORM_SPEC);
    }

    public void setTransformSpec(String value) {
        set(KEY_TRANSFORM_SPEC, value);
    }

    public PublicShipmentMetadata transformSpec(String value) {
        return set(KEY_TRANSFORM_SPEC, value);
    }

    public byte[] getMasterFinalSeed() {
        return getBytes(KEY_MASTER_FINAL_SEED);
    }

    public void setMasterFinalSeed(byte[] value) {
        setBytes(KEY_MASTER_FINAL_SEED, value);
    }

    public PublicShipmentMetadata masterFinalSeed(byte[] value) {
        return setBytes(KEY_MASTER_FINAL_SEED, value);
    }

    public byte[] getMasterTransformSeed() {
        return getBytes(KEY_MASTER_TRANSFORM_SEED);
    }

    public void setMasterTransformSeed(byte[] value) {
        setBytes(KEY_MASTER_TRANSFORM_SEED, value);
    }

    public PublicShipmentMetadata masterTransformSeed(byte[] value) {
        return setBytes(KEY_MASTER_TRANSFORM_SEED, value);
    }

    public byte[] getChallengeFinalSeed() {
        return getBytes(KEY_CHALLENGE_FINAL_SEED);
    }

    public void setChallengeFinalSeed(byte[] value) {
        setBytes(KEY_CHALLENGE_FINAL_SEED, value);
    }

    public PublicShipmentMetadata challengeFinalSeed(byte[] value) {
        return setBytes(KEY_CHALLENGE_FINAL_SEED, value);
    }

    public byte[] getChallengeTransformSeed() {
        return getBytes(KEY_CHALLENGE_TRANSFORM_SEED);
    }

    public void setChallengeTransformSeed(byte[] value) {
        setBytes(KEY_CHALLENGE_TRANSFORM_SEED, value);
    }

    public PublicShipmentMetadata challengeTransformSeed(byte[] value) {
        return setBytes(KEY_CHALLENGE_TRANSFORM_SEED, value);
    }

    public byte[] getIv() {
        return getBytes(KEY_IV);
    }

    public void setIv(byte[] iv) {
        setBytes(KEY_IV, iv);
    }

    public PublicShipmentMetadata iv(byte[] iv) {
        return setBytes(KEY_IV, iv);
    }

    public byte[] getEncryptedDataSignature() {
        return getBytes(KEY_ENCRYPTED_DATA_SIGNATURE);
    }

    public void setEncryptedDataSignature(byte[] iv) {
        setBytes(KEY_ENCRYPTED_DATA_SIGNATURE, iv);
    }

    public PublicShipmentMetadata encryptedDataSignature(byte[] iv) {
        return setBytes(KEY_ENCRYPTED_DATA_SIGNATURE, iv);
    }

    public String getDescription() {
        return get(KEY_DESCRIPTION);
    }

    public void setDescription(String description) {
        set(KEY_DESCRIPTION, description);
    }

    public PublicShipmentMetadata description(String description) {
        return set(KEY_DESCRIPTION, description);
    }

    public String getIconUrl() {
        return get(KEY_ICON_URL);
    }

    public void setIconUrl(String iconUrl) {
        set(KEY_ICON_URL, iconUrl);
    }

    public PublicShipmentMetadata iconUrl(String iconUrl) {
        return set(KEY_ICON_URL, iconUrl);
    }

    public String getPasswordNormalization() {
        return get(KEY_PASSWORD_NORMALIZATION);
    }

    public void setPasswordNormalization(String value) {
        set(KEY_PASSWORD_NORMALIZATION, value);
    }

    public PublicShipmentMetadata passwordNormalization(String value) {
        return set(KEY_PASSWORD_NORMALIZATION, value);
    }

    public String getPasswordMask() {
        return get(KEY_PASSWORD_MASK);
    }

    public void setPasswordMask(String value) {
        set(KEY_PASSWORD_MASK, value);
    }

    public PublicShipmentMetadata passwordMask(String value) {
        return set(KEY_PASSWORD_MASK, value);
    }

}
