package eu.bindworks.mcourier;

import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonReader;
import javax.json.JsonWriter;
import java.io.StringReader;
import java.io.StringWriter;

public interface BytesAndJsonSerializable {
    byte[] serializeToBytes();

    void deserializeFromBytes(byte[] bytes, int offset, int length);

    JsonObject serializeToJsonObject();

    void deserializeFromJsonObject(JsonObject object);

    default void deserializeFromBytes(byte[] bytes) {
        deserializeFromBytes(bytes, 0, bytes.length);
    }

    default String serializeToJson() {
        StringWriter os = new StringWriter();
        try (JsonWriter writer = Json.createWriter(os)) {
            writer.writeObject(serializeToJsonObject());
            return os.toString();
        }
    }

    default void deserializeFromJson(String json) {
        JsonReader reader = Json.createReader(new StringReader(json));
        JsonObject jsonObject = reader.readObject();
        deserializeFromJsonObject(jsonObject);
    }
}
