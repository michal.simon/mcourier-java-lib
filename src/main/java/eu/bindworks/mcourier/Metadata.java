package eu.bindworks.mcourier;

import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;
import javax.json.JsonString;
import javax.json.JsonValue;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Base64;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public abstract class Metadata<T extends Metadata<T>> implements Iterable<String>, BytesAndJsonSerializable {
    public static final int MAXIMUM_KEY_LENGTH = 16;
    public static final int MAXIMUM_VALUE_LENGTH = 1024;
    public static final int MAXIMUM_ENTRIES = 128;

    protected Map<String, String> data = new HashMap<>();

    public T set(String key, String value) {
        data.put(key, value);
        if (data.size() > MAXIMUM_ENTRIES) {
            throw new MCourierException.EncryptionDecryptionException("Too many entries in metadata");
        }
        return (T) this;
    }

    public String get(String key) {
        return data.get(key);
    }

    public boolean validateSyntax() {
        return true;
    }

    public boolean isString(String key) {
        return data.containsKey(key);
    }

    public boolean isInt(String key) {
        return getInt(key) != null;
    }

    public boolean isLong(String key) {
        return getLong(key) != null;
    }

    public boolean isBytes(String key) {
        return getBytes(key) != null;
    }

    public boolean isBoolean(String key) {
        return getBoolean(key) != null;
    }

    public Integer getInt(String key) {
        String stringValue = get(key);
        if (stringValue == null) {
            return null;
        }
        try {
            return Integer.parseInt(stringValue);
        } catch (NumberFormatException e) {
            return null;
        }
    }

    public T setInt(String key, Integer value) {
        if (value == null) {
            return set(key, null);
        }
        return set(key, String.valueOf(value));
    }

    public Long getLong(String key) {
        String stringValue = get(key);
        if (stringValue == null) {
            return null;
        }
        try {
            return Long.parseLong(stringValue);
        } catch (NumberFormatException e) {
            return null;
        }
    }

    public T setLong(String key, Long value) {
        if (value == null) {
            return set(key, null);
        }
        return set(key, String.valueOf(value));
    }

    public byte[] getBytes(String key) {
        String stringValue = get(key);
        if (stringValue == null) {
            return null;
        }
        try {
            return Base64.getUrlDecoder().decode(stringValue);
        } catch (IllegalArgumentException e) {
            return null;
        }
    }

    public T setBytes(String key, byte[] value) {
        if (value == null) {
            return set(key, null);
        }
        return set(key, Base64.getUrlEncoder().encodeToString(value));
    }

    public Boolean getBoolean(String key) {
        if (!containsKeys(key)) {
            return null;
        }
        return getInt(key) == 0;
    }

    public T setBoolean(String key, Boolean value) {
        return setInt(key, value == null ? null : (value ? 1 : 0));
    }

    public boolean containsKeys(String... keys) {
        return data.keySet().containsAll(Arrays.asList(keys));
    }

    public T clone() {
        T other = construct();
        other.data = new HashMap<>(data);
        return other;
    }

    protected abstract T construct();
    protected abstract int getSerializationSignature();

    @Override
    public Iterator<String> iterator() {
        return data.keySet().iterator();
    }

    public Map<String, String> getVisibleData() {
        return data.entrySet().stream()
                .filter(e -> !e.getKey().startsWith("_"))
                .collect(Collectors.toUnmodifiableMap(Map.Entry::getKey, Map.Entry::getValue));
    }

    @Override
    public byte[] serializeToBytes() {
        List<String> keys = new ArrayList<>(data.keySet());
        keys.sort(Comparator.naturalOrder());
        return MCourierException.translate(() -> {
            ByteArrayOutputStream os = new ByteArrayOutputStream();
            DataOutputStream dos = new DataOutputStream(os);
            dos.writeInt(getSerializationSignature());
            for (String key: keys) {
                String value = data.get(key);
                if (value == null) {
                    continue;
                }
                byte[] keyBytes = key.getBytes(StandardCharsets.UTF_8);
                if (keyBytes.length > MAXIMUM_KEY_LENGTH) {
                    throw new MCourierException.EncryptionDecryptionException("Cannot serialize key " + key + " it's too long");
                }
                byte[] valueBytes = value.getBytes(StandardCharsets.UTF_8);
                if (valueBytes.length > MAXIMUM_VALUE_LENGTH) {
                    throw new MCourierException.EncryptionDecryptionException("Cannot serialize value " + value + " it's too long");
                }
                dos.writeInt(keyBytes.length);
                dos.write(keyBytes);
                dos.writeInt(valueBytes.length);
                dos.write(valueBytes);
            }
            dos.writeInt(-1);
            dos.flush();
            return os.toByteArray();
        });
    }

    @Override
    public void deserializeFromBytes(byte[] bytes, int offset, int length) {
        Map<String, String> newData = new HashMap<>();
        MCourierException.translate(() -> {
            ByteArrayInputStream is = new ByteArrayInputStream(bytes, offset, length);
            DataInputStream dis = new DataInputStream(is);
            int signature = dis.readInt();
            if (signature != getSerializationSignature()) {
                throw new MCourierException.EncryptionDecryptionException("Metadata signature expected " + getSerializationSignature() + " but got " + signature);
            }
            while (true) {
                int keyLength = dis.readInt();
                if (keyLength == -1) {
                    break;
                }
                if (keyLength < 1 || keyLength > MAXIMUM_KEY_LENGTH) {
                    throw new MCourierException.EncryptionDecryptionException("Metadata key length " + keyLength + " too long or short");
                }
                byte[] keyData = new byte[keyLength];
                dis.readFully(keyData);
                int valueLength = dis.readInt();
                if (valueLength < 0 || valueLength > MAXIMUM_VALUE_LENGTH) {
                    throw new MCourierException.EncryptionDecryptionException("Metadata value length " + valueLength + " too long or short");
                }
                byte[] valueData = new byte[valueLength];
                dis.readFully(valueData);

                String key = new String(keyData, StandardCharsets.UTF_8);
                String value = new String(valueData, StandardCharsets.UTF_8);
                newData.put(key, value);

                if (newData.size() > MAXIMUM_ENTRIES) {
                    throw new MCourierException.EncryptionDecryptionException("Metadata has too many entries");
                }
            }
        });
        data = newData;
    }

    @Override
    public JsonObject serializeToJsonObject() {
        JsonObjectBuilder objectBuilder = Json.createObjectBuilder();
        objectBuilder.add("_", getSerializationSignature());
        for (Map.Entry<String, String> e: data.entrySet()) {
            objectBuilder.add(e.getKey(), e.getValue());
        }
        return objectBuilder.build();
    }

    @Override
    public void deserializeFromJsonObject(JsonObject jsonObject) {
        if (jsonObject.containsKey("_") && jsonObject.getInt("_") != getSerializationSignature()) {
            throw new MCourierException.EncryptionDecryptionException("Metadata signature expected " + getSerializationSignature() + " but got " + jsonObject.get("_"));
        }

        Map<String, String> newData = new HashMap<>();
        for (Map.Entry<String, JsonValue> e: jsonObject.entrySet()) {
            if (e.getKey().length() > MAXIMUM_KEY_LENGTH) {
                throw new MCourierException.EncryptionDecryptionException("Metadata key length " + e.getKey() + " too long or short");
            }
            if ("_".equals(e.getKey())) {
                continue;
            }
            if (!(e.getValue() instanceof JsonString)) {
                continue;
            }
            String value = ((JsonString) e.getValue()).getString();
            if (value.length() > MAXIMUM_VALUE_LENGTH) {
                throw new MCourierException.EncryptionDecryptionException("Metadata value length " + value + " too long or short");
            }

            newData.put(e.getKey(), value);
            if (newData.size() > MAXIMUM_ENTRIES) {
                throw new MCourierException.EncryptionDecryptionException("Metadata has too many entries");
            }
        }
        data = newData;
    }

    @Override
    public int hashCode() {
        return -1;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (!obj.getClass().equals(getClass())) {
            return false;
        }
        Metadata<?> m = (Metadata<?>) obj;
        return m.data.equals(data);
    }
}
