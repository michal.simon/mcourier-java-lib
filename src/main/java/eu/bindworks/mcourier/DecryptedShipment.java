package eu.bindworks.mcourier;

import eu.bindworks.mcourier.crypto.EncryptionSuite;

import java.io.InputStream;
import java.util.Arrays;
import java.util.List;

public class DecryptedShipment {
    private EncryptionSuite encryptionSuite;

    private PublicShipmentMetadata publicShipmentMetadata;
    private HostingShipmentMetadata hostingShipmentMetadata;

    private SecretShipmentMetadata secretShipmentMetadata;
    private SecretFileMetadata[] secretFileMetadata;

    public DecryptedShipment(EncryptionSuite encryptionSuite, PublicShipmentMetadata publicShipmentMetadata, HostingShipmentMetadata hostingShipmentMetadata, SecretShipmentMetadata secretShipmentMetadata, SecretFileMetadata[] secretFileMetadata) {
        this.encryptionSuite = encryptionSuite;
        this.publicShipmentMetadata = publicShipmentMetadata;
        this.hostingShipmentMetadata = hostingShipmentMetadata;
        this.secretShipmentMetadata = secretShipmentMetadata;
        this.secretFileMetadata = secretFileMetadata;
    }

    public InputStream decryptFile(InputStream inputStream, int fileIndex) {
        SecretFileMetadata fmd = this.secretFileMetadata[fileIndex];
        return encryptionSuite.decryptStream(fmd.getFilePassword(), fmd.getFileIv(), inputStream);
    }

    public PublicShipmentMetadata getPublicShipmentMetadata() {
        return publicShipmentMetadata;
    }

    public HostingShipmentMetadata getHostingShipmentMetadata() {
        return hostingShipmentMetadata;
    }

    public SecretShipmentMetadata getSecretShipmentMetadata() {
        return secretShipmentMetadata;
    }

    public int getFilesCount() {
        return secretShipmentMetadata.getFilesCount();
    }

    public SecretFileMetadata getFileMetadata(int i) {
        return secretFileMetadata[i];
    }

    public List<SecretFileMetadata> getAllFileMetadata() {
        return Arrays.asList(secretFileMetadata);
    }
}
