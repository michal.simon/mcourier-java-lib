package eu.bindworks.mcourier;

public class HostingShipmentMetadata extends Metadata<HostingShipmentMetadata> {
    public static final int SIGNATURE = Utility.fourLetterCode("mChs");

    public static final String KEY_CHALLENGES_COUNT = "_chc";
    public static final String KEY_CHALLENGE_PREFIX = "_ch";
    public static final String KEY_CHALLENGE_RESPONSE_PREFIX = "_chr";
    public static final String KEY_DISABLE_SERVER_DECRYPTION = "_dsd";

    public static final String KEY_EXPIRES_ON = "expiresOn";
    public static final String KEY_TEMPLATE = "template";

    @Override
    protected HostingShipmentMetadata construct() {
        return new HostingShipmentMetadata();
    }

    @Override
    public boolean validateSyntax() {
        if (!super.validateSyntax()
                || !isInt(KEY_CHALLENGES_COUNT)
                || !isLong(KEY_EXPIRES_ON)
                || (containsKeys(KEY_DISABLE_SERVER_DECRYPTION) && !isBoolean(KEY_DISABLE_SERVER_DECRYPTION))) {
            return false;
        }

        int count = getChallengesCount();
        for (int i=0; i<count; i++) {
            if (!isBytes(KEY_CHALLENGE_PREFIX + i)
                || !isBytes(KEY_CHALLENGE_RESPONSE_PREFIX + i)) {
                return false;
            }
        }

        return true;
    }

    @Override
    protected int getSerializationSignature() {
        return SIGNATURE;
    }

    public String getTemplate() {
        return get(KEY_TEMPLATE);
    }

    public void setTemplate(String template) {
        set(KEY_TEMPLATE, template);
    }

    public HostingShipmentMetadata template(String template) {
        return set(KEY_TEMPLATE, template);
    }

    public boolean isDisableServerDecryption() {
        Boolean b = getBoolean(KEY_DISABLE_SERVER_DECRYPTION);
        return b != null && b;
    }

    public void setDisableServerDecryption(boolean value) {
        setBoolean(KEY_DISABLE_SERVER_DECRYPTION, value);
    }

    public HostingShipmentMetadata disableServerDecryption(boolean value) {
        return setBoolean(KEY_DISABLE_SERVER_DECRYPTION, value);
    }

    public Long getExpiresOn() {
        return getLong(KEY_EXPIRES_ON);
    }

    public void setExpiresOn(Long value) {
        setLong(KEY_EXPIRES_ON, value);
    }

    public HostingShipmentMetadata expiresOn(Long value) {
        return setLong(KEY_EXPIRES_ON, value);
    }

    public Integer getChallengesCount() {
        return getInt(KEY_CHALLENGES_COUNT);
    }

    public byte[] getChallenge(int i) {
        return getBytes(KEY_CHALLENGE_PREFIX + i);
    }

    public byte[] getChallengeResponse(int i) {
        return getBytes(KEY_CHALLENGE_RESPONSE_PREFIX + i);
    }

    public HostingShipmentMetadata addChallengeResponse(byte[] challenge, byte[] response) {
        Integer challengesCount = getInt(KEY_CHALLENGES_COUNT);
        if (challengesCount == null) {
            challengesCount = 0;
        }
        setBytes(KEY_CHALLENGE_PREFIX + challengesCount.toString(), challenge);
        setBytes(KEY_CHALLENGE_RESPONSE_PREFIX + challengesCount.toString(), response);
        challengesCount += 1;
        setInt(KEY_CHALLENGES_COUNT, challengesCount);
        return this;
    }
}
