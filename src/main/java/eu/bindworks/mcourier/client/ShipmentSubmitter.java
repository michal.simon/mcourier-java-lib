package eu.bindworks.mcourier.client;

import eu.bindworks.mcourier.MCourierException;
import eu.bindworks.mcourier.crypto.EncryptedShipmentBuilder;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.ssl.SSLContextBuilder;
import org.apache.http.ssl.SSLContexts;

import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.security.SecureRandom;
import java.util.List;

public class ShipmentSubmitter {
    private ShipmentSubmitterConfiguration configuration;

    public ShipmentSubmitter(ShipmentSubmitterConfiguration configuration) {
        this.configuration = configuration;
    }

    public String submit(EncryptedShipmentBuilder encryptedShipmentBuilder) throws IOException {
        MultipartEntityBuilder builder = MultipartEntityBuilder.create()
                .setMode(HttpMultipartMode.BROWSER_COMPATIBLE)
                .addBinaryBody("e",
                        encryptedShipmentBuilder.getEncryptedShipment().serializeToJson().getBytes(StandardCharsets.UTF_8),
                        ContentType.APPLICATION_OCTET_STREAM,
                        "e");

        List<InputStream> inputStreams = encryptedShipmentBuilder.getInputStreams();
        for (InputStream is: inputStreams) {
            builder.addBinaryBody("f",
                    is,
                    ContentType.APPLICATION_OCTET_STREAM,
                    "f");
        }
        HttpEntity requestEntity = builder.build();

        RequestConfig requestConfig = RequestConfig.custom()
                .setExpectContinueEnabled(true)
                .build();
        HttpPost httpPost = new HttpPost(configuration.getBaseUri().resolve("/upload/v1/upload/encrypted"));
        httpPost.setEntity(requestEntity);
        httpPost.setConfig(requestConfig);

        byte[] responseBytes;
        try (CloseableHttpClient httpClient = createHttpClient()) {
            HttpResponse response = httpClient.execute(httpPost);
            try (InputStream is = response.getEntity().getContent()) {
                responseBytes = is.readAllBytes();
            }
        }

        try (JsonReader jsonReader = Json.createReader(new ByteArrayInputStream(responseBytes))) {
            JsonObject responseObject = jsonReader.readObject();
            return responseObject.getString("downloadUrl");
        } catch (Exception e) {
            throw new MCourierException.ShipmentSubmissionException(new String(responseBytes, StandardCharsets.UTF_8));
        }
    }

    private CloseableHttpClient createHttpClient() {
        return MCourierException.translate(() -> {
            SSLContextBuilder sslContext = SSLContexts.custom();
            // sslContext.setProtocol("TLSv1.2");
            // sslContext.setProvider(EncryptionSuite.BOUNCY_CASTLE_JSSE_PROVIDER);
            sslContext.setSecureRandom(new SecureRandom());
            if (configuration.getKeyStore() != null) {
                sslContext.loadKeyMaterial(configuration.getKeyStore(), configuration.getKeyStorePassword());
            }
            sslContext.loadTrustMaterial(configuration.getTrustStore(), null);
            HttpClientBuilder httpClient = HttpClients.custom()
                    .setSSLContext(sslContext.build());
            return httpClient.build();
        });
    }
}
