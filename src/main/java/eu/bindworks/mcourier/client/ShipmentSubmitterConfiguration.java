package eu.bindworks.mcourier.client;

import eu.bindworks.mcourier.MCourierException;

import java.net.URI;
import java.security.KeyStore;

public class ShipmentSubmitterConfiguration {
    public static final URI PRODUCTION_BASE_URI = URI.create("https://upload.mcourier.eu/");
    public static final URI DEV_BASE_URI = URI.create("https://upload.mcourier.dev.bindworks.eu/");
    public static final KeyStore PRODUCTION_TRUST_STORE = loadTrustStore("mCourier_Production_Server_ROOT_CA.p12");
    public static final KeyStore DEV_TRUST_STORE = loadTrustStore("mCourier_Test_Server_ROOT_CA.p12");

    private URI baseUri = PRODUCTION_BASE_URI;
    private KeyStore keyStore;
    private char[] keyStorePassword;
    private String keyStoreAlias;
    private KeyStore trustStore = PRODUCTION_TRUST_STORE;
    private char[] trustStorePassword = new char[0];

    public URI getBaseUri() {
        return baseUri;
    }

    public void setBaseUri(URI baseUri) {
        this.baseUri = baseUri;
    }

    public KeyStore getKeyStore() {
        return keyStore;
    }

    public void setKeyStore(KeyStore keyStore) {
        this.keyStore = keyStore;
    }

    public char[] getKeyStorePassword() {
        return keyStorePassword;
    }

    public void setKeyStorePassword(char[] keyStorePassword) {
        this.keyStorePassword = keyStorePassword;
    }

    public String getKeyStoreAlias() {
        return keyStoreAlias;
    }

    public void setKeyStoreAlias(String keyStoreAlias) {
        this.keyStoreAlias = keyStoreAlias;
    }

    public KeyStore getTrustStore() {
        return trustStore;
    }

    public void setTrustStore(KeyStore trustStore) {
        this.trustStore = trustStore;
    }

    public char[] getTrustStorePassword() {
        return trustStorePassword;
    }

    public void setTrustStorePassword(char[] trustStorePassword) {
        this.trustStorePassword = trustStorePassword;
    }

    public static KeyStore loadTrustStore(String name) {
        return MCourierException.translate(() -> {
            KeyStore ks = KeyStore.getInstance("pkcs12");
            ks.load(ShipmentSubmitterConfiguration.class.getResourceAsStream(name), new char[0]);
            return ks;
        });
    }
}
