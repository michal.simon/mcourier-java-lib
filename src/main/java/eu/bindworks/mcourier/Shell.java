package eu.bindworks.mcourier;

import eu.bindworks.mcourier.client.ShipmentSubmitter;
import eu.bindworks.mcourier.client.ShipmentSubmitterConfiguration;
import eu.bindworks.mcourier.crypto.ClearableBytes;
import eu.bindworks.mcourier.crypto.EncryptedShipmentBuilder;
import eu.bindworks.mcourier.crypto.EncryptionSuite;
import eu.bindworks.mcourier.crypto.PasswordGenerator;
import eu.bindworks.mcourier.crypto.PasswordGeneratorConfiguration;
import eu.bindworks.mcourier.crypto.PasswordNormalizerBuilder;
import eu.bindworks.mcourier.crypto.RandomnessProvider;
import eu.bindworks.mcourier.crypto.SecureRandomRandomnessProvider;
import eu.bindworks.mcourier.crypto.ShipmentEncryptor;
import eu.bindworks.mcourier.crypto.ShipmentEncryptorConfiguration;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.nio.charset.StandardCharsets;
import java.security.KeyStore;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.BiConsumer;

@SuppressWarnings({"java:S125", "java:S106"})
public class Shell {
    private static final String PARAM_BASE_URI = "--base-uri";
    private static final String PARAM_TRUST_STORE = "--trust-store";
    private static final String PARAM_CLIENT_PKCS_12 = "--client-pkcs12";
    private static final String PARAM_CLIENT_PKCS_12_PASSWORD = "--client-pkcs12-password";
    private static final String PARAM_TRANSFORM_SPEC = "--transform-spec";
    private static final String PARAM_PASSWORD = "--password";
    private static final String PARAM_PASSWORD_LENGTH = "--password-length";
    private static final String PARAM_DESCRIPTION = "--description";
    private static final String PARAM_TEMPLATE = "--template";
    private static final String PARAM_EXPIRATION = "--expiration";
    private static final String PARAM_SHIPMENT_META = "--shipment-meta";
    private static final String PARAM_HOSTING_META = "--hosting-meta";
    private static final String PARAM_SECRET_META = "--secret-meta";
    private static final String PARAM_FILE_DESCRIPTION = "--file-description";
    private static final String PARAM_FILE_META = "--file-meta";
    private static final String PARAM_OUTPUT = "--output";

    private static final String ACTION_UPLOAD = "upload";
    private static final String ACTION_ENCRYPT = "encrypt";
    private static final String ACTION_DECRYPT = "decrypt";
    private static final String ACTION_BENCHMARK = "benchmark";

    public void main0(String[] argv) {
        CommandLineParser parser = new CommandLineParser();
        parser.parseArguments(argv);

        switch (parser.action) {
            case UPLOAD: this.uploadShipment(parser); break;
            case ENCRYPT: this.encryptShipment(parser); break;
            case BENCHMARK: this.benchmark(parser); break;
            // case DECRYPT: this.decryptShipment(parser); break;
            default: exit(1, "No command specified");
        }
    }

    private void encryptShipment(CommandLineParser parser) {
        encryptShipmentAnd(parser, (builder, passwordBytes) -> MCourierException.translate(() -> {
            List<InputStream> inputStreams = builder.getInputStreams();
            int index = 0;
            for (InputStream is: inputStreams) {
                try (FileOutputStream os = new FileOutputStream(String.format("%s.%d", parser.output, index++))) {
                    is.transferTo(os);
                } finally {
                    try { is.close(); } catch (IOException ex) { /* silently ignore */ }
                }
            }

            String fileName = String.format("%s.json", parser.output);
            try (FileOutputStream os = new FileOutputStream(fileName)) {
                os.write(builder.getEncryptedShipment().serializeToJson().getBytes(StandardCharsets.UTF_8));
            }

            // vytisteni URL a hesla
            System.out.write(fileName.getBytes(StandardCharsets.UTF_8));
            System.out.write('\t');
            System.out.write(passwordBytes.getData());
            System.out.write('\n');
        }));
    }

    private void uploadShipment(CommandLineParser parser) {
        encryptShipmentAnd(parser, (builder, passwordBytes) -> MCourierException.translate(() -> {
            ShipmentSubmitter shipmentSubmitter = new ShipmentSubmitter(parser.shipmentSubmitterConfiguration);

            // odeslani zasilky a ziskani URL kde ji najdu. pokud zde vidite pouze hash a ne
            // cele url, tak jsem to jeste na serveru neopravil, verte, ze bude chodit URL.
            String url = shipmentSubmitter.submit(builder);

            // vytisteni URL a hesla
            System.out.write(url.getBytes(StandardCharsets.UTF_8));
            System.out.write('\t');
            System.out.write(passwordBytes.getData());
            System.out.write('\n');
        }));
    }

    private void benchmark(CommandLineParser parser) {
        MCourierException.translate(() -> {
            String transformSpec = parser.shipmentEncryptorConfiguration.getTransformSpec();
            EncryptionSuite suite = parser.shipmentEncryptorConfiguration.getEncryptionSuite();
            PasswordGenerator passwordGenerator = new PasswordGenerator(parser.passwordGeneratorConfiguration);
            RandomnessProvider randomnessProvider = new SecureRandomRandomnessProvider();
            ClearableBytes password = passwordGenerator.generatePassword();
            byte[] transformSeed = randomnessProvider.randomBytes(suite.getTransformSeedLength());
            byte[] finalSeed = randomnessProvider.randomBytes(suite.getTransformSeedLength());

            int rounds = 100;
            System.out.println(String.format("Generating master key %d times...", rounds));
            long startMillis = System.currentTimeMillis();
            for (int i=0; i<rounds; i++) {
                suite.generateMasterKey(password.getData(), transformSeed, finalSeed, transformSpec);
            }
            long endMillis = System.currentTimeMillis();

            double seconds = (endMillis - startMillis) / 1000.0;
            double days = seconds / (60 * 60 * 24);
            double years = seconds / (60 * 60 * 24 * 365);
            System.out.println(String.format("Average speed: %.4f s/key = %.4f keys/s = %1.1e keys/day = %1.1e keys/year",
                    seconds / rounds,
                    rounds / seconds,
                    rounds / days,
                    rounds / years));

            double combinations = Math.pow(PasswordNormalizerBuilder.NONMIXABLE_CHARACTERS.length(), parser.passwordGeneratorConfiguration.getPasswordLength());
            System.out.println(String.format("Average time to bruteforce %d length password out of %d characters (%1.1e combinations): %1.1e days / %1.1e years",
                    parser.passwordGeneratorConfiguration.getPasswordLength(),
                    PasswordNormalizerBuilder.NONMIXABLE_CHARACTERS.length(),
                    combinations,
                    combinations / (rounds / days),
                    combinations / (rounds / years)));
        });
    }

    private interface EncryptedShipmentConsumer {
        void accept(EncryptedShipmentBuilder builder, ClearableBytes password);
    }

    private void encryptShipmentAnd(CommandLineParser parser, EncryptedShipmentConsumer consumer) {
        ShipmentEncryptor shipmentEncryptor = new ShipmentEncryptor(parser.shipmentEncryptorConfiguration);
        PasswordGenerator passwordGenerator = new PasswordGenerator(parser.passwordGeneratorConfiguration);

        // Zahajujeme balicek. Je treba uzavrit do try(), protoze drzi IO prostredky a je treba
        // je zarucene na konci uvolnit
        try (EncryptedShipmentBuilder builder = shipmentEncryptor.createShipment()) {
            // prirazeni metadat
            builder.setPublicShipmentMetadata(parser.publicShipmentMetadata);
            builder.setHostingShipmentMetadata(parser.hostingShipmentMetadata);
            builder.setSecretShipmentMetadata(parser.secretShipmentMetadata);

            for (FileData fd: parser.fileData) {
                // pro kazdy soubor je vygenerovan klic a otevren sifrovaci stream

                /* Typicke nastaveni pro soubor: */

                // SecretFileMetadata sfm = new SecretFileMetadata();

                //   nastaveni popisu souboru
                // sfm.setDescription("Popis");

                //   nastaveni nazvu souboru -- pokud neni doplneno a je znamo z File, tak se doplni
                // sfm.setFilename("nazev.txt");

                //   nastaveni content type -- pokud neni doplneno a je detekovatelne z File, tak se doplni
                // sfm.setContentType("text/plain");

                //   nastaveni ikony -- pouzivejte pouze nazvy souboru bez cesty
                // sfm.setIconUrl("txt.jpg");

                // zde mame strukturu vyplnenou uz z casu cteni argumentu z prikazove radky, takze
                // ji staci nastavit
                builder.encryptFile(fd.file, fd.meta);
            }

            // vygeneruji heslo. doporucuji pouzivat proste
            //   passwordGenerator.generatePassword()
            //
            // zde dovolujeme uzivateli heslo specifikovat i na prikazove radce
            try (ClearableBytes passwordBytes = new ClearableBytes()) {
                if (parser.password == null) {
                    passwordBytes.replaceTake(passwordGenerator.generatePassword());
                    parser.publicShipmentMetadata.passwordNormalization(passwordGenerator.getNormalizerSequence());
                } else {
                    passwordBytes.replaceTake(parser.password.getBytes(StandardCharsets.UTF_8));
                }
//                =
//            } parser.password == null
//                    ? passwordGenerator.generatePassword()
//                    : ClearableBytes.copy(parser.password.getBytes(StandardCharsets.UTF_8))) {

                // uzavreme zasilku prislusnym heslem a nastavime expiraci
                builder.finalizeShipment(passwordBytes.getData(), parser.hostingShipmentMetadata.getExpiresOn());

                //
                // zde je zasilka pripravena. vsechny klice jsou pripraveny, hlavicka cele zasilky je jiz
                // vytvorena a zasifrovana.
                // builder stale drzi soubory otevrene, protoze se jeste nezacalo ve skutecnosti cist.
                // se ctenim se zacne az prijde na prislusny soubor rada pri prenosu.
                //

                consumer.accept(builder, passwordBytes);
            }
        }

    }

    public static class CommandLineParser {
        ShipmentSubmitterConfiguration shipmentSubmitterConfiguration = new ShipmentSubmitterConfiguration();
        ShipmentEncryptorConfiguration shipmentEncryptorConfiguration = new ShipmentEncryptorConfiguration();
        PasswordGeneratorConfiguration passwordGeneratorConfiguration = new PasswordGeneratorConfiguration();
        PublicShipmentMetadata publicShipmentMetadata = new PublicShipmentMetadata();
        HostingShipmentMetadata hostingShipmentMetadata = new HostingShipmentMetadata();
        SecretShipmentMetadata secretShipmentMetadata = new SecretShipmentMetadata();
        List<FileData> fileData = new ArrayList<>();
        String password = null;
        String output = "shipment";
        String clientPkcs12 = null;
        String clientPkcs12Password = null;
        Action action = Action.UNKNOWN;

        public void parseArguments(String[] argv) {
            /* Typicke nastaveni */

            // nastaveni popisu balicku, objevi se v property "description" v template
            // publicShipmentMetadata.setDescription("Popis balicku");

            // nastaveni data do kdy lze balicek vyzvednout
            hostingShipmentMetadata.setExpiresOn(System.currentTimeMillis() + hours2Millis(48));

            // nastaveni template, kterym se bude na serveru renderovat stranka. zatim nema vliv
            // hostingShipmentMetadata.setTemplate("covid");

            /* Nastaveni pro fajnsmekry, ted prosim nepouzivat, nakonec se nejspis bude tahat ze serveru */

            // RandomnessProvider provider = new SecureRandomRandomnessProvider();

            //   nastaveni specifickeho RNG -- bude potreba?
            // shipmentEncryptorConfiguration.setRandomnessProvider(provider);

            //   nastaveni slozitosti sifry
            // shipmentEncryptorConfiguration.setArgon2TransformSpec(32768, 3, 1);

            //   nastaveni poctu paru challenge-response ktere se vygeneruji do zasilky
            // shipmentEncryptorConfiguration.setChallengesCount(4);

            //   nastaveni specifickeho RNG -- bude potreba?
            // passwordGeneratorConfiguration.setRandomnessProvider(provider);

            //   nastaveni delky hesla -- primo ovlivnuje prolomitelnost archivu
            // passwordGeneratorConfiguration.setPasswordLength(12);

            //   priznak, zda je povoleno desifrovat zasilku na serveru
            // hostingShipmentMetadata.setDisableServerDecryption(false);

            int argumentIndex = 0;
            while (argumentIndex < argv.length) {
                boolean isOption = argv[argumentIndex].startsWith("--");
                if (isOption && argumentIndex + 1 >= argv.length) {
                    exit(1, "Missing option %s parameter\n", argv[argumentIndex]);
                }

                String parameter = isOption ? argv[argumentIndex+1] : "";
                if (isOption && !action.allowedOptions.contains(argv[argumentIndex])) {
                    exit(1, "Invalid option %s\n", argv[argumentIndex]);
                }

                switch (argv[argumentIndex]) {
                    case PARAM_BASE_URI: shipmentSubmitterConfiguration.setBaseUri(URI.create(parameter)); break;
                    case PARAM_TRUST_STORE: loadTrustStore(parameter); break;
                    case PARAM_CLIENT_PKCS_12: clientPkcs12 = parameter; break;
                    case PARAM_CLIENT_PKCS_12_PASSWORD: clientPkcs12Password = parameter; break;

                    case PARAM_TRANSFORM_SPEC: shipmentEncryptorConfiguration.setTransformSpec(parameter); break;

                    case PARAM_OUTPUT: output = parameter; break;

                    case PARAM_PASSWORD: password = parameter; break;
                    case PARAM_PASSWORD_LENGTH: passwordGeneratorConfiguration.setPasswordLength(Integer.parseInt(parameter)); break;
                    case PARAM_DESCRIPTION: publicShipmentMetadata.setDescription(parameter); break;
                    case PARAM_TEMPLATE: hostingShipmentMetadata.setTemplate(parameter); break;
                    case PARAM_EXPIRATION: hostingShipmentMetadata.setExpiresOn(System.currentTimeMillis() + hours2Millis(Integer.parseInt(parameter))); break;
                    case PARAM_SHIPMENT_META: setMeta(parameter, publicShipmentMetadata::set); break;
                    case PARAM_HOSTING_META: setMeta(parameter, hostingShipmentMetadata::set); break;
                    case PARAM_SECRET_META: setMeta(parameter, secretShipmentMetadata::set); break;

                    case PARAM_FILE_DESCRIPTION:
                    case PARAM_FILE_META:
                        if (fileData.isEmpty()) {
                            exit(1, "%s can only be used after a file name", argv[argumentIndex]);
                        }
                        switch (argv[argumentIndex]) {
                            case PARAM_FILE_DESCRIPTION: fileData.get(fileData.size()-1).meta.setDescription(parameter); break;
                            case PARAM_FILE_META: setMeta(parameter, fileData.get(fileData.size()-1).meta::set); break;
                            default: throw new IllegalStateException();
                        }
                        break;

                    default:
                        if (isOption) {
                            exit(1, "Unknown argument %s", argv[argumentIndex]);
                        }

                        if (action.supportsActions) {
                            action = Action.forName(argv[argumentIndex]);
                        } else if (action.supportsFiles) {
                            File f = new File(argv[argumentIndex]);
                            if (!f.canRead() || !f.exists() || !f.isFile()) {
                                exit(2, "File not found: %s", f);
                            }
                            fileData.add(new FileData(f));
                        } else {
                            exit(1, "Unknown argument %s", argv[argumentIndex]);
                        }

                        break;
                }

                argumentIndex += isOption ? 2 : 1;
            }

            if (action == Action.UNKNOWN) {
                exit(1, "No action specified");
            }

            if (action.supportsFiles && fileData.isEmpty()) {
                exit(1, "No files to ship specified.");
            }

            if (clientPkcs12 != null) {
                loadPkcs12ClientCertificate(clientPkcs12,
                        clientPkcs12Password == null ? null : clientPkcs12Password.toCharArray());
            }
        }

        private void loadPkcs12ClientCertificate(String file, char[] password) {
            MCourierException.translate(() -> {
                KeyStore ks = KeyStore.getInstance("pkcs12");
                ks.load(new FileInputStream(file), password);
                shipmentSubmitterConfiguration.setKeyStore(ks);
                shipmentSubmitterConfiguration.setKeyStorePassword(password);
            });
        }

        private void loadTrustStore(String file) {
            if ("production".equals(file)) {
                shipmentSubmitterConfiguration.setTrustStore(ShipmentSubmitterConfiguration.PRODUCTION_TRUST_STORE);
                return;
            } else if ("dev".equals(file)) {
                shipmentSubmitterConfiguration.setTrustStore(ShipmentSubmitterConfiguration.DEV_TRUST_STORE);
                return;
            }
            MCourierException.translate(() -> {
                KeyStore ks = KeyStore.getInstance("pkcs12");
                ks.load(new FileInputStream(file), new char[0]);
                shipmentSubmitterConfiguration.setTrustStore(ks);
            });
        }

        private void setMeta(String str, BiConsumer<String, String> setter) {
            int equalsIndex = str.indexOf('=');
            if (equalsIndex == -1) {
                exit(1, "Meta needs to be name=value\n");
            }
            String name = str.substring(0, equalsIndex).trim();
            String value = str.substring(equalsIndex+1).trim();
            setter.accept(name, value);
        }

        private static long hours2Millis(int hours) {
            return hours * 60 * 60 * (long) 1000;
        }
    }

    private static void exit(int errorCode, String message, Object ...args) {
        System.err.printf(message, args);
        System.exit(errorCode);
    }

    public static class FileData {
        final File file;
        SecretFileMetadata meta = new SecretFileMetadata();

        public FileData(File file) {
            this.file = file;
        }
    }

    public enum Action {
        UNKNOWN(false, true, new String[] { }),
        UPLOAD(true, false, new String[] { PARAM_BASE_URI, PARAM_TRUST_STORE, PARAM_CLIENT_PKCS_12, PARAM_CLIENT_PKCS_12_PASSWORD, PARAM_TRANSFORM_SPEC, PARAM_PASSWORD, PARAM_DESCRIPTION, PARAM_TEMPLATE, PARAM_EXPIRATION, PARAM_SHIPMENT_META, PARAM_HOSTING_META, PARAM_SECRET_META, PARAM_FILE_DESCRIPTION, PARAM_FILE_META, PARAM_PASSWORD_LENGTH }),
        ENCRYPT(true, false, new String[] { PARAM_TRANSFORM_SPEC, PARAM_PASSWORD, PARAM_DESCRIPTION, PARAM_TEMPLATE, PARAM_EXPIRATION, PARAM_SHIPMENT_META, PARAM_HOSTING_META, PARAM_SECRET_META, PARAM_FILE_DESCRIPTION, PARAM_FILE_META, PARAM_OUTPUT, PARAM_PASSWORD_LENGTH }),
        DECRYPT(false, false, new String[] {  }),
        BENCHMARK(false, false, new String[] { PARAM_TRANSFORM_SPEC, PARAM_PASSWORD_LENGTH });

        final boolean supportsFiles;
        final boolean supportsActions;
        final List<String> allowedOptions;

        Action(boolean supportsFiles, boolean supportsActions, String[] allowedOptions) {
            this.supportsFiles = supportsFiles;
            this.supportsActions = supportsActions;
            this.allowedOptions = Arrays.asList(allowedOptions);
        }

        public static Action forName(String name) {
            switch (name) {
                case ACTION_UPLOAD:
                    return Action.UPLOAD;
                case ACTION_ENCRYPT:
                    return Action.ENCRYPT;
                case ACTION_DECRYPT:
                    return Action.DECRYPT;
                case ACTION_BENCHMARK:
                    return Action.BENCHMARK;
                default:
                    throw new IllegalArgumentException(name);
            }
        }
    }

    public static void main(String[] argv) {
        new Shell().main0(argv);
    }
}
