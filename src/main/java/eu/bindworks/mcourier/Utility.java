package eu.bindworks.mcourier;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

public class Utility {
    public static int fourLetterCode(String letters) {
        if (letters == null || letters.length() != 4) {
            throw new IllegalArgumentException(letters);
        }
        int total = 0;
        for (int i=0; i<4; i++) {
            total <<= 8;
            total += (byte) letters.charAt(i);
        }
        return total;
    }

    public static byte[] encodeIntegers(int... integers) {
        try {
            ByteArrayOutputStream os = new ByteArrayOutputStream();
            DataOutputStream dos = new DataOutputStream(os);
            for (int i: integers) {
                dos.writeInt(i);
            }
            dos.close();
            return os.toByteArray();
        } catch (IOException e) {
            throw new RuntimeException("Error writing to ByteArrayOutputStream");
        }
    }

    public static int[] decodeIntegers(byte[] data) {
        if (data.length % 4 != 0) {
            throw new IllegalArgumentException("decodeIntegers() only works on arrays of length multiple of 4");
        }
        try {
            int count = data.length / 4;
            int[] result = new int[count];
            ByteArrayInputStream is = new ByteArrayInputStream(data);
            DataInputStream dis = new DataInputStream(is);
            for (int i=0; i<count; i++) {
                result[i] = dis.readInt();
            }
            return result;
        } catch (IOException e) {
            throw new RuntimeException("Error reading from ByteArrayInputStream");
        }
    }

    public static boolean isInt(String str) {
        try {
            Integer.parseInt(str);
            return true;
        } catch (NumberFormatException e) {
            return false;
        }
    }
}
