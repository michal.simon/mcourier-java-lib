# Klientská knihovna mCourier

[![Codeac.io](https://static.codeac.io/badges/3-21968482.svg "Codeac")](https://app.codeac.io/gitlab/bindworks/mcourier%2Fmcourier-java-lib)

## Konfigurace maven

```xml
<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 https://maven.apache.org/xsd/maven-4.0.0.xsd">
...
    <dependencies>
        <dependency>
            <groupId>eu.bindworks.mcourier</groupId>
            <artifactId>mcourier-encryption-lib</artifactId>
            <version>1.0.0-SNAPSHOT</version>
        </dependency>
...
    </dependencies>
...
    <repositories>
        <repository>
            <id>gitlab-maven</id>
            <url>https://gitlab.com/api/v4/groups/9805310/-/packages/maven</url>
        </repository>
    </repositories>
</project>
```

## Použití z příkazové řádky

```bash
cd target
java -jar mcourier-encryption-lib-*.jar \
  --description "Popis" \
  document.pdf \
  --file-description "Popis dokumentu"
``` 

## Použití z kódu

Lze vyčíst z [SubmitCmd](src/main/java/eu/bindworks/mcourier/SubmitCmd.java).
